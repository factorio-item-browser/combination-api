package main

import (
	"context"
	"flag"
	"log/slog"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	awsconfig "github.com/aws/aws-sdk-go-v2/config"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/combinationid"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/config"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/event"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/handler"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/log"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/storage"
)

// Example command: go run cmd/event/main.go -source="factorio-item-browser.export.status.test" -payload='{"combinationID":"2f4a45fa-a509-a9d1-aae6-ffcf984a7a76","status":"test","timestamp":"2038-01-19T03:14:07Z"}'

func main() {
	logger := log.New(config.ForLogger())

	// Initialize AWS config
	awsCfg, err := awsconfig.LoadDefaultConfig(context.Background())
	if err != nil {
		logger.Error(log.MessageAWSConfigError, slog.String("error", err.Error()))
		os.Exit(1)
	}

	// Initialize dependencies
	storageService := storage.NewService(config.ForStorage(), awsCfg)
	calculator := combinationid.NewCalculator(storageService)

	// Initialize event handlers
	eventHandlers := []event.Handler{
		event.NewErrorHandler(calculator, storageService),
		event.NewStatusHandler(calculator, storageService),
		event.NewSuccessHandler(calculator, storageService),
	}

	handle := handler.NewEventHandler(eventHandlers).HandleCloudWatchEvent
	decoratedHandle := log.NewMiddleware(logger, handle)

	if config.ForLambda().IsLambda {
		// We are running inside a lambda function.
		lambda.Start(decoratedHandle)
		return
	}

	// We are running directly on the commandline.
	var source string
	var payload string
	flag.StringVar(&source, "source", "", "The source of the event to handle.")
	flag.StringVar(&payload, "payload", "", "The payload of the event to handle.")
	flag.Parse()

	_, _ = decoratedHandle(context.Background(), events.CloudWatchEvent{
		Source: source,
		Detail: []byte(payload),
	})
}

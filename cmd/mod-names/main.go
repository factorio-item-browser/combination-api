package main

import (
	"context"
	"encoding/json"
	"flag"
	"log/slog"
	"os"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	awsconfig "github.com/aws/aws-sdk-go-v2/config"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/combinationid"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/config"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/handler"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/log"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/request"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/response"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/storage"
)

// Example command: go run cmd/mod-names/main.go -mods=base

func main() {
	logger := log.New(config.ForLogger())

	// Initialize AWS config
	awsCfg, err := awsconfig.LoadDefaultConfig(context.Background())
	if err != nil {
		logger.Error(log.MessageAWSConfigError, slog.String("error", err.Error()))
		os.Exit(1)
	}

	// Initialize dependencies
	storageService := storage.NewService(config.ForStorage(), awsCfg)
	calculator := combinationid.NewCalculator(storageService)

	handle := handler.NewCombinationHandler(calculator, storageService).HandleModNames
	decoratedHandle := response.NewProxyTransformMiddleware(request.NewTransformProxyMiddleware(log.NewMiddleware(logger, handle)))
	if config.ForLambda().IsLambda {
		// We are running inside a lambda function.
		lambda.Start(decoratedHandle)
		return
	}

	// We are running directly on the commandline.
	var modNames string
	flag.StringVar(&modNames, "mods", "", "The comma separated list of mod names.")
	flag.Parse()

	requestBody, _ := json.Marshal(strings.Split(modNames, ","))
	proxyRequest := events.APIGatewayProxyRequest{
		Body: string(requestBody),
	}

	proxyResponse, _ := decoratedHandle(context.Background(), proxyRequest)
	logger.Info(log.MessageResponse, slog.String("body", proxyResponse.Body), slog.Int("status", proxyResponse.StatusCode))
}

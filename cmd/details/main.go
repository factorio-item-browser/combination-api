package main

import (
	"context"
	"flag"
	"log/slog"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	awsconfig "github.com/aws/aws-sdk-go-v2/config"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/combinationid"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/config"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/handler"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/log"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/request"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/response"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/storage"
)

// Example command: go run cmd/details/main.go -id=2f4a45fa-a509-a9d1-aae6-ffcf984a7a76

func main() {
	logger := log.New(config.ForLogger())

	// Initialize AWS config
	awsCfg, err := awsconfig.LoadDefaultConfig(context.Background())
	if err != nil {
		logger.Error(log.MessageAWSConfigError, slog.String("error", err.Error()))
		os.Exit(1)
	}

	// Initialize dependencies
	storageService := storage.NewService(config.ForStorage(), awsCfg)
	calculator := combinationid.NewCalculator(storageService)

	handle := handler.NewCombinationHandler(calculator, storageService).HandleDetails
	decoratedHandle := response.NewProxyTransformMiddleware(request.NewTransformProxyMiddleware(log.NewMiddleware(logger, handle)))
	if config.ForLambda().IsLambda {
		// We are running inside a lambda function.
		lambda.Start(decoratedHandle)
		return
	}

	// We are running directly on the commandline.
	var combinationID string
	flag.StringVar(&combinationID, "id", "", "The combination id to return the details of.")
	flag.Parse()

	proxyRequest := events.APIGatewayProxyRequest{
		PathParameters: map[string]string{
			"combinationID": combinationID,
		},
	}

	proxyResponse, _ := decoratedHandle(context.Background(), proxyRequest)
	logger.Info(log.MessageResponse, slog.String("body", proxyResponse.Body), slog.Int("status", proxyResponse.StatusCode))
}

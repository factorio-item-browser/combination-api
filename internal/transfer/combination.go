package transfer

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

type Combination struct {
	ID            CombinationID `dynamodbav:"ID"`
	Name          string        `dynamodbav:"Name,omitempty"`
	ModNames      []string      `dynamodbav:"ModNames,stringset"`
	LastUsageTime time.Time     `dynamodbav:"LastUsageTime"`

	ValidationTime     *time.Time                         `dynamodbav:"ValidationTime,omitempty"`
	ValidatedMods      map[string]modportalclient.Version `dynamodbav:"ValidatedMods,omitempty"`
	ValidationProblems []ValidationProblem                `dynamodbav:"ValidationProblems,omitempty"`

	ExportStatus  map[string]time.Time               `dynamodbav:"ExportStatus,omitempty"`
	ExportTime    *time.Time                         `dynamodbav:"ExportTime,omitempty"`
	ExportError   *ExportError                       `dynamodbav:"ExportError,omitempty"`
	ExportedMods  map[string]modportalclient.Version `dynamodbav:"ExportedMods,omitempty"`
	ExportNumbers map[string]uint64                  `dynamodbav:"ExportNumbers,omitempty"`
	ExportFiles   map[string]ExportFile              `dynamodbav:"ExportFiles,omitempty"`
}

type ValidationProblemType string

const (
	UnknownMod        ValidationProblemType = "unknownMod"
	NoRelease         ValidationProblemType = "noRelease"
	MissingDependency ValidationProblemType = "missingDependency"
	Conflict          ValidationProblemType = "conflict"
)

type ValidationProblem struct {
	ModName    string                      `dynamodbav:"ModName"`
	Type       ValidationProblemType       `dynamodbav:"Type"`
	Dependency *modportalclient.Dependency `dynamodbav:"Dependency,omitempty"`
}

type ExportError struct {
	Type    string
	Message string
}

func (e ExportError) MarshalText() ([]byte, error) {
	return []byte(e.Type + "#" + e.Message), nil
}

func (e *ExportError) UnmarshalText(data []byte) error {
	parts := append(strings.SplitN(string(data), "#", 2), "", "")
	e.Type = parts[0]
	e.Message = parts[1]
	return nil
}

type ExportFile struct {
	Size           uint64
	CompressedSize uint64
}

func (f ExportFile) MarshalText() ([]byte, error) {
	return []byte(fmt.Sprintf("%d#%d", f.Size, f.CompressedSize)), nil
}

func (f *ExportFile) UnmarshalText(data []byte) error {
	parts := append(strings.SplitN(string(data), "#", 2), "", "")
	f.Size, _ = strconv.ParseUint(parts[0], 10, 64)
	f.CompressedSize, _ = strconv.ParseUint(parts[1], 10, 64)
	return nil
}

package transfer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExportError_MarshalText(t *testing.T) {
	expectedResult := []byte("error-type#error message")

	instance := ExportError{
		Type:    "error-type",
		Message: "error message",
	}
	result, err := instance.MarshalText()

	assert.NoError(t, err)
	assert.Equal(t, expectedResult, result)
}

func TestExportError_UnmarshalText(t *testing.T) {
	tests := map[string]struct {
		data            string
		expectedType    string
		expectedMessage string
	}{
		"happy path": {
			data:            "error-type#error message",
			expectedType:    "error-type",
			expectedMessage: "error message",
		},
		"single value": {
			data:            "error-type",
			expectedType:    "error-type",
			expectedMessage: "",
		},
		"empty value": {
			data:            "",
			expectedType:    "",
			expectedMessage: "",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := ExportError{}
			err := instance.UnmarshalText([]byte(test.data))

			assert.NoError(t, err)
			assert.Equal(t, test.expectedType, instance.Type)
			assert.Equal(t, test.expectedMessage, instance.Message)
		})
	}
}

func TestExportFile_MarshalText(t *testing.T) {
	expectedResult := []byte("1337#42")

	instance := ExportFile{
		Size:           1337,
		CompressedSize: 42,
	}
	result, err := instance.MarshalText()

	assert.NoError(t, err)
	assert.Equal(t, expectedResult, result)
}

func TestExportFile_UnmarshalText(t *testing.T) {
	tests := map[string]struct {
		data                   string
		expectedSize           uint64
		expectedCompressedSize uint64
	}{
		"happy path": {
			data:                   "1337#42",
			expectedSize:           1337,
			expectedCompressedSize: 42,
		},
		"single value": {
			data:                   "1337",
			expectedSize:           1337,
			expectedCompressedSize: 0,
		},
		"empty value": {
			data:                   "",
			expectedSize:           0,
			expectedCompressedSize: 0,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := ExportFile{}
			err := instance.UnmarshalText([]byte(test.data))

			assert.NoError(t, err)
			assert.Equal(t, test.expectedSize, instance.Size)
			assert.Equal(t, test.expectedCompressedSize, instance.CompressedSize)
		})
	}
}

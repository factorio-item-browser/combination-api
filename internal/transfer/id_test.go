package transfer

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestNewCombinationID(t *testing.T) {
	id := uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76")

	instance := NewCombinationID(id)

	assert.Equal(t, id, instance.id)
}

func TestCombinationID_String(t *testing.T) {
	tests := map[string]struct {
		id             uuid.UUID
		expectedResult string
	}{
		"vanilla": {
			id:             uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"),
			expectedResult: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
		},
		"all zero": {
			id:             uuid.MustParse("00000000-0000-0000-0000-000000000000"),
			expectedResult: "00000000-0000-0000-0000-000000000000",
		},
		"single one": {
			id:             uuid.MustParse("00000000-0000-0000-0000-000000000001"),
			expectedResult: "00000000-0000-0000-0000-000000000001",
		},
		"all f": {
			id:             uuid.MustParse("ffffffff-ffff-ffff-ffff-ffffffffffff"),
			expectedResult: "ffffffff-ffff-ffff-ffff-ffffffffffff",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := CombinationID{id: test.id}
			result := instance.String()

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestCombinationID_Short(t *testing.T) {
	tests := map[string]struct {
		id             uuid.UUID
		expectedResult string
	}{
		"vanilla": {
			id:             uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"),
			expectedResult: "1reA6H5z4uFpotvegbLIr4",
		},
		"all zero": {
			id:             uuid.MustParse("00000000-0000-0000-0000-000000000000"),
			expectedResult: "0000000000000000000000",
		},
		"single one": {
			id:             uuid.MustParse("00000000-0000-0000-0000-000000000001"),
			expectedResult: "0000000000000000000001",
		},
		"all f": {
			id:             uuid.MustParse("ffffffff-ffff-ffff-ffff-ffffffffffff"),
			expectedResult: "7N42dgm5tFLK9N8MT7fHC7",
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			instance := CombinationID{id: test.id}
			result := instance.Short()

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestCombinationID_MarshalText(t *testing.T) {
	id := uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76")
	expectedResult := []byte("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76")

	instance := CombinationID{id: id}
	result, err := instance.MarshalText()

	assert.NoError(t, err)
	assert.Equal(t, expectedResult, result)
}

func TestCombinationID_UnmarshalText(t *testing.T) {
	data := []byte("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76")
	expectedResult := CombinationID{id: uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76")}

	var instance CombinationID
	err := instance.UnmarshalText(data)

	assert.NoError(t, err)
	assert.Equal(t, expectedResult, instance)
}

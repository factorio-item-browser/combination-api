package transfer

import (
	"fmt"
	"math/big"

	"github.com/google/uuid"
)

type CombinationID struct {
	id uuid.UUID
}

func NewCombinationID(id uuid.UUID) CombinationID {
	return CombinationID{
		id: id,
	}
}

func (id CombinationID) String() string {
	return id.id.String()
}

func (id CombinationID) Short() string {
	var i big.Int
	i.SetBytes(id.id[:])
	return fmt.Sprintf("%022s", i.Text(62))
}

func (id CombinationID) MarshalText() ([]byte, error) {
	return id.id.MarshalText()
}

func (id *CombinationID) UnmarshalText(data []byte) error {
	return (*id).id.UnmarshalText(data)
}

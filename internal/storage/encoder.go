package storage

import "github.com/aws/aws-sdk-go-v2/feature/dynamodb/attributevalue"

var encoder = attributevalue.NewEncoder(func(opts *attributevalue.EncoderOptions) {
	opts.UseEncodingMarshalers = true
})

var decoder = attributevalue.NewDecoder(func(opts *attributevalue.DecoderOptions) {
	opts.UseEncodingUnmarshalers = true
})

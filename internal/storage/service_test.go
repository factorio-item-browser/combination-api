package storage

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/aws/smithy-go"
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/config"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/storage/mocks"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

func TestNewService(t *testing.T) {
	tableName := "foo"

	cfg := config.Storage{
		TableName: tableName,
	}
	instance := NewService(cfg, aws.Config{})

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.table)
	assert.Equal(t, tableName, instance.tableName)
}

func TestService_CombinationByID(t *testing.T) {
	combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
	lastUsageTime := time.Date(2038, 1, 19, 3, 14, 7, 123000000, time.UTC)
	item := map[string]types.AttributeValue{
		"ID":            &types.AttributeValueMemberS{Value: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"},
		"ModNames":      &types.AttributeValueMemberSS{Value: []string{"foo", "bar"}},
		"LastUsageTime": &types.AttributeValueMemberS{Value: "2038-01-19T03:14:07.123Z"},
	}

	tests := map[string]struct {
		getOutput         *dynamodb.GetItemOutput
		getError          error
		expectedErrorType error
		expectedResult    *transfer.Combination
	}{
		"happy path": {
			getOutput: &dynamodb.GetItemOutput{
				Item: item,
			},
			getError:          nil,
			expectedErrorType: nil,
			expectedResult: &transfer.Combination{
				ID:            combinationID,
				ModNames:      []string{"foo", "bar"},
				LastUsageTime: lastUsageTime,
			},
		},
		"without item": {
			getOutput: &dynamodb.GetItemOutput{
				Item: nil,
			},
			getError:          nil,
			expectedErrorType: nil,
			expectedResult:    nil,
		},
		"with error": {
			getOutput:         nil,
			getError:          fmt.Errorf("test error"),
			expectedErrorType: &errors.StorageError{},
			expectedResult:    nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			tableName := "table"

			expectedInput := dynamodb.GetItemInput{
				TableName: aws.String(tableName),
				Key: map[string]types.AttributeValue{
					"ID": &types.AttributeValueMemberS{Value: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"},
				},
			}

			table := mocks.NewTable(t)
			table.On("GetItem", ctx, &expectedInput).Once().Return(test.getOutput, test.getError)

			instance := Service{
				table:     table,
				tableName: tableName,
			}
			result, err := instance.CombinationByID(ctx, combinationID)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestService_CombinationIDByName(t *testing.T) {
	combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
	item := map[string]types.AttributeValue{
		"ID":            &types.AttributeValueMemberS{Value: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"},
		"ModNames":      &types.AttributeValueMemberSS{Value: []string{"foo", "bar"}},
		"LastUsageTime": &types.AttributeValueMemberS{Value: "2038-01-19T03:14:07.123Z"},
	}

	tests := map[string]struct {
		combinationName   string
		expectQuery       bool
		queryOutput       *dynamodb.QueryOutput
		queryError        error
		expectedErrorType error
		expectedResult    *transfer.CombinationID
	}{
		"happy path": {
			combinationName: "fancy",
			expectQuery:     true,
			queryOutput: &dynamodb.QueryOutput{
				Items: []map[string]types.AttributeValue{
					item,
				},
			},
			queryError:        nil,
			expectedErrorType: nil,
			expectedResult:    &combinationID,
		},
		"without name": {
			combinationName:   "",
			expectQuery:       false,
			expectedErrorType: nil,
			expectedResult:    nil,
		},
		"without items": {
			combinationName: "fancy",
			expectQuery:     true,
			queryOutput: &dynamodb.QueryOutput{
				Items: []map[string]types.AttributeValue{},
			},
			queryError:        nil,
			expectedErrorType: nil,
			expectedResult:    nil,
		},
		"with error": {
			combinationName:   "fancy",
			expectQuery:       true,
			queryOutput:       nil,
			queryError:        fmt.Errorf("test error"),
			expectedErrorType: &errors.StorageError{},
			expectedResult:    nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			tableName := "table"
			indexName := "index"

			expectedInput := dynamodb.QueryInput{
				TableName: aws.String(tableName),
				IndexName: aws.String(indexName),
				ExpressionAttributeNames: map[string]string{
					"#0": "Name",
				},
				ExpressionAttributeValues: map[string]types.AttributeValue{
					":0": &types.AttributeValueMemberS{Value: test.combinationName},
				},
				KeyConditionExpression: aws.String("#0 = :0"),
				Limit:                  aws.Int32(1),
			}

			table := mocks.NewTable(t)
			if test.expectQuery {
				table.On("Query", ctx, &expectedInput).Once().Return(test.queryOutput, test.queryError)
			}

			instance := Service{
				table:     table,
				tableName: tableName,
				indexName: indexName,
			}
			result, err := instance.CombinationIDByName(ctx, test.combinationName)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestService_Persist(t *testing.T) {
	tests := map[string]struct {
		putError          error
		expectedErrorType error
	}{
		"happy path": {
			putError:          nil,
			expectedErrorType: nil,
		},
		"with error": {
			putError:          fmt.Errorf("test error"),
			expectedErrorType: &errors.StorageError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			tableName := "table"
			combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
			modNames := []string{"foo", "bar"}

			combination := transfer.Combination{
				ID:            combinationID,
				ModNames:      modNames,
				LastUsageTime: time.Date(2038, 1, 19, 3, 14, 7, 0, time.UTC),
			}

			expectedInput := dynamodb.PutItemInput{
				TableName: aws.String(tableName),
				Item: map[string]types.AttributeValue{
					"ID":            &types.AttributeValueMemberS{Value: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"},
					"ModNames":      &types.AttributeValueMemberSS{Value: modNames},
					"LastUsageTime": &types.AttributeValueMemberS{Value: "2038-01-19T03:14:07Z"},
				},
			}

			table := mocks.NewTable(t)
			table.On("PutItem", ctx, &expectedInput).Once().Return(nil, test.putError)

			instance := Service{
				table:     table,
				tableName: tableName,
			}
			err := instance.Persist(ctx, combination)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}

func TestService_PersistLastUsageTime(t *testing.T) {
	tests := map[string]struct {
		updateError error
	}{
		"happy path": {
			updateError: nil,
		},
		"with error": {
			updateError: fmt.Errorf("test error"),
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			tableName := "table"
			combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))

			combination := transfer.Combination{
				ID:            combinationID,
				LastUsageTime: time.Date(2038, 1, 19, 3, 14, 7, 0, time.UTC),
			}

			expectedInput := dynamodb.UpdateItemInput{
				TableName: aws.String(tableName),
				Key: map[string]types.AttributeValue{
					"ID": &types.AttributeValueMemberS{Value: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"},
				},
				ExpressionAttributeNames: map[string]string{
					"#0": "LastUsageTime",
				},
				ExpressionAttributeValues: map[string]types.AttributeValue{
					":0": &types.AttributeValueMemberS{Value: "2038-01-19T03:14:07Z"},
				},
				UpdateExpression: aws.String("SET #0 = :0\n"),
			}

			table := mocks.NewTable(t)
			table.On("UpdateItem", ctx, &expectedInput).Once().Return(nil, test.updateError)

			instance := Service{
				table:     table,
				tableName: tableName,
			}
			instance.PersistLastUsageTime(ctx, combination)
		})
	}
}

func TestService_PersistValidation(t *testing.T) {
	tests := map[string]struct {
		updateError       error
		expectedErrorType error
	}{
		"happy path": {
			updateError:       nil,
			expectedErrorType: nil,
		},
		"with error": {
			updateError:       fmt.Errorf("test error"),
			expectedErrorType: &errors.StorageError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			dependency := modportalclient.NewDependency("bar")

			ctx := context.Background()
			tableName := "table"
			combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
			validationTime := time.Date(2038, 1, 18, 3, 14, 7, 0, time.UTC)

			combination := transfer.Combination{
				ID:             combinationID,
				LastUsageTime:  time.Date(2038, 1, 19, 3, 14, 7, 0, time.UTC),
				ValidationTime: &validationTime,
				ValidatedMods: map[string]modportalclient.Version{
					"foo": modportalclient.NewVersion("1.2.3"),
				},
				ValidationProblems: []transfer.ValidationProblem{
					{
						ModName:    "foo",
						Type:       transfer.Conflict,
						Dependency: &dependency,
					},
				},
			}

			expectedInput := dynamodb.UpdateItemInput{
				TableName: aws.String(tableName),
				Key: map[string]types.AttributeValue{
					"ID": &types.AttributeValueMemberS{Value: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"},
				},
				ExpressionAttributeNames: map[string]string{
					"#0": "LastUsageTime",
					"#1": "ValidationTime",
					"#2": "ValidatedMods",
					"#3": "ValidationProblems",
				},
				ExpressionAttributeValues: map[string]types.AttributeValue{
					":0": &types.AttributeValueMemberS{Value: "2038-01-19T03:14:07Z"},
					":1": &types.AttributeValueMemberS{Value: "2038-01-18T03:14:07Z"},
					":2": &types.AttributeValueMemberM{Value: map[string]types.AttributeValue{
						"foo": &types.AttributeValueMemberS{Value: "1.2.3"},
					}},
					":3": &types.AttributeValueMemberL{Value: []types.AttributeValue{
						&types.AttributeValueMemberM{Value: map[string]types.AttributeValue{
							"ModName":    &types.AttributeValueMemberS{Value: "foo"},
							"Type":       &types.AttributeValueMemberS{Value: "conflict"},
							"Dependency": &types.AttributeValueMemberS{Value: "bar"},
						}},
					}},
				},
				UpdateExpression: aws.String("SET #0 = :0, #1 = :1, #2 = :2, #3 = :3\n"),
			}

			table := mocks.NewTable(t)
			table.On("UpdateItem", ctx, &expectedInput).Once().Return(nil, test.updateError)

			instance := Service{
				table:     table,
				tableName: tableName,
			}
			err := instance.PersistValidation(ctx, combination)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}

func TestService_PersistExport(t *testing.T) {
	tests := map[string]struct {
		updateError       error
		expectedErrorType error
	}{
		"happy path": {
			updateError:       nil,
			expectedErrorType: nil,
		},
		"with error": {
			updateError:       fmt.Errorf("test error"),
			expectedErrorType: &errors.StorageError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			tableName := "table"
			combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
			exportTime := time.Date(2038, 1, 19, 3, 14, 7, 0, time.UTC)

			combination := transfer.Combination{
				ID: combinationID,
				ExportError: &transfer.ExportError{
					Type:    "test type",
					Message: "test error",
				},
				ExportTime: &exportTime,
				ExportedMods: map[string]modportalclient.Version{
					"foo": modportalclient.NewVersion("1.2.3"),
				},
				ExportNumbers: map[string]uint64{
					"item": 42,
				},
				//ExportFiles: map[string]transfer.ExportFile{
				//	"data": {
				//		Size:           7331,
				//		CompressedSize: 1337,
				//	},
				//},
			}

			expectedInput := dynamodb.UpdateItemInput{
				TableName: aws.String(tableName),
				Key: map[string]types.AttributeValue{
					"ID": &types.AttributeValueMemberS{Value: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"},
				},
				ExpressionAttributeNames: map[string]string{
					"#0": "ExportError",
					"#1": "ExportTime",
					"#2": "ExportedMods",
					"#3": "ExportNumbers",
					//"#4": "ExportFiles",
				},
				ExpressionAttributeValues: map[string]types.AttributeValue{
					":0": &types.AttributeValueMemberS{Value: "test type#test error"},
					":1": &types.AttributeValueMemberS{Value: "2038-01-19T03:14:07Z"},
					":2": &types.AttributeValueMemberM{Value: map[string]types.AttributeValue{
						"foo": &types.AttributeValueMemberS{Value: "1.2.3"},
					}},
					":3": &types.AttributeValueMemberM{Value: map[string]types.AttributeValue{
						"item": &types.AttributeValueMemberN{Value: "42"},
					}},
					//":4": &types.AttributeValueMemberM{Value: map[string]types.AttributeValue{
					//	"data": &types.AttributeValueMemberM{Value: map[string]types.AttributeValue{
					//		"Síze":           &types.AttributeValueMemberN{Value: "7331"},
					//		"CompressedSíze": &types.AttributeValueMemberN{Value: "1337"},
					//	}},
					//}},
				},
				UpdateExpression: aws.String("SET #0 = :0, #1 = :1, #2 = :2, #3 = :3\n"),
			}

			table := mocks.NewTable(t)
			table.On("UpdateItem", ctx, &expectedInput).Once().Return(nil, test.updateError)

			instance := Service{
				table:     table,
				tableName: tableName,
			}
			err := instance.PersistExport(ctx, combination)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}

func TestService_PersistExportStatus(t *testing.T) {
	tests := map[string]struct {
		updateError          error
		expectFallbackUpdate bool
		fallbackUpdateError  error
		expectedErrorType    error
	}{
		"happy path": {
			updateError:          nil,
			expectFallbackUpdate: false,
			fallbackUpdateError:  nil,
			expectedErrorType:    nil,
		},
		"without export status": {
			updateError:          &smithy.GenericAPIError{Code: "ConditionalCheckFailedException"},
			expectFallbackUpdate: true,
			fallbackUpdateError:  nil,
			expectedErrorType:    nil,
		},
		"without combination": {
			updateError:          &smithy.GenericAPIError{Code: "ConditionalCheckFailedException"},
			expectFallbackUpdate: true,
			fallbackUpdateError:  &smithy.GenericAPIError{Code: "ConditionalCheckFailedException"},
			expectedErrorType:    nil,
		},
		"update error": {
			updateError:          fmt.Errorf("test error"),
			expectFallbackUpdate: false,
			fallbackUpdateError:  nil,
			expectedErrorType:    &errors.StorageError{},
		},
		"fallback update error": {
			updateError:          &smithy.GenericAPIError{Code: "ConditionalCheckFailedException"},
			expectFallbackUpdate: true,
			fallbackUpdateError:  fmt.Errorf("test error"),
			expectedErrorType:    &errors.StorageError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			tableName := "table"
			combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
			status := "status"
			timestamp := time.Date(2038, 1, 19, 3, 14, 7, 0, time.UTC)

			expectedInput := dynamodb.UpdateItemInput{
				TableName: aws.String(tableName),
				Key: map[string]types.AttributeValue{
					"ID": &types.AttributeValueMemberS{Value: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"},
				},
				ExpressionAttributeNames: map[string]string{
					"#0": "ExportStatus",
					"#1": "status",
				},
				ExpressionAttributeValues: map[string]types.AttributeValue{
					":0": &types.AttributeValueMemberS{Value: "2038-01-19T03:14:07Z"},
				},
				UpdateExpression:    aws.String("SET #0.#1 = :0\n"),
				ConditionExpression: aws.String("attribute_exists (#0)"),
			}

			expectedFallbackInput := dynamodb.UpdateItemInput{
				TableName: aws.String(tableName),
				Key: map[string]types.AttributeValue{
					"ID": &types.AttributeValueMemberS{Value: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"},
				},
				ExpressionAttributeNames: map[string]string{
					"#0": "ID",
					"#1": "ExportStatus",
				},
				ExpressionAttributeValues: map[string]types.AttributeValue{
					":0": &types.AttributeValueMemberS{Value: combinationID.String()},
					":1": &types.AttributeValueMemberM{Value: map[string]types.AttributeValue{
						"status": &types.AttributeValueMemberS{Value: "2038-01-19T03:14:07Z"},
					}},
				},
				UpdateExpression:    aws.String("SET #1 = :1\n"),
				ConditionExpression: aws.String("#0 = :0"),
			}

			table := mocks.NewTable(t)
			table.On("UpdateItem", ctx, &expectedInput).Once().Return(nil, test.updateError)
			if test.expectFallbackUpdate {
				table.On("UpdateItem", ctx, &expectedFallbackInput).Once().Return(nil, test.fallbackUpdateError)
			}

			instance := Service{
				table:     table,
				tableName: tableName,
			}
			err := instance.PersistExportStatus(ctx, combinationID, status, timestamp)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}

package storage

import (
	"context"
	errors2 "errors"
	"fmt"
	"log/slog"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/feature/dynamodb/expression"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb"
	"github.com/aws/aws-sdk-go-v2/service/dynamodb/types"
	"github.com/aws/smithy-go"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/config"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/log"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
)

type table interface {
	GetItem(ctx context.Context, params *dynamodb.GetItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.GetItemOutput, error)
	PutItem(ctx context.Context, params *dynamodb.PutItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.PutItemOutput, error)
	Query(ctx context.Context, params *dynamodb.QueryInput, optFns ...func(*dynamodb.Options)) (*dynamodb.QueryOutput, error)
	UpdateItem(ctx context.Context, params *dynamodb.UpdateItemInput, optFns ...func(*dynamodb.Options)) (*dynamodb.UpdateItemOutput, error)
}

type Service struct {
	table table

	tableName string
	indexName string
}

func NewService(cfg config.Storage, awsCfg aws.Config) *Service {
	return &Service{
		table:     dynamodb.NewFromConfig(awsCfg),
		tableName: cfg.TableName,
		indexName: cfg.IndexName,
	}
}

// CombinationByID fetches the combination with the provided id.
func (s *Service) CombinationByID(
	ctx context.Context,
	combinationID transfer.CombinationID,
) (*transfer.Combination, error) {
	input := dynamodb.GetItemInput{
		TableName: aws.String(s.tableName),
		Key: map[string]types.AttributeValue{
			"ID": &types.AttributeValueMemberS{Value: combinationID.String()},
		},
	}

	output, err := s.table.GetItem(ctx, &input)
	if err != nil {
		return nil, errors.NewStorageError(err)
	}
	if output.Item == nil {
		return nil, nil
	}

	var combination transfer.Combination
	_ = decoder.Decode(&types.AttributeValueMemberM{Value: output.Item}, &combination)
	return &combination, nil
}

// CombinationIDByName fetches and returns the ID of the combination with the provided name.
func (s *Service) CombinationIDByName(ctx context.Context, name string) (*transfer.CombinationID, error) {
	if name == "" {
		return nil, nil
	}

	expr, _ := expression.NewBuilder().WithKeyCondition(
		expression.Key("Name").Equal(expression.Value(name)),
	).Build()

	input := dynamodb.QueryInput{
		TableName:                 aws.String(s.tableName),
		IndexName:                 aws.String(s.indexName),
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		KeyConditionExpression:    expr.KeyCondition(),
		Limit:                     aws.Int32(1),
	}

	output, err := s.table.Query(ctx, &input)
	if err != nil {
		return nil, errors.NewStorageError(err)
	}
	if len(output.Items) == 0 {
		return nil, nil
	}

	var combination transfer.Combination
	_ = decoder.Decode(&types.AttributeValueMemberM{Value: output.Items[0]}, &combination)

	return &combination.ID, nil
}

func (s *Service) Persist(ctx context.Context, combination transfer.Combination) error {
	item, _ := encoder.Encode(combination)

	input := dynamodb.PutItemInput{
		TableName: aws.String(s.tableName),
		Item:      item.(*types.AttributeValueMemberM).Value,
	}
	_, err := s.table.PutItem(ctx, &input)
	if err != nil {
		return errors.NewStorageError(err)
	}
	return nil
}

// PersistLastUsageTime will persist only the LastUsageTime of the provided combination. The timestamp must already be
// updated.
func (s *Service) PersistLastUsageTime(ctx context.Context, combination transfer.Combination) {
	err := s.persistPartial(ctx, combination, []string{"LastUsageTime"})
	if err != nil {
		log.FromContext(ctx).Error(
			"update last usage time error",
			slog.String("error", err.Error()),
			slog.String("combinationID", combination.ID.String()),
		)
	}
}

// PersistValidation will persist the validation fields of the provided combination.
func (s *Service) PersistValidation(ctx context.Context, combination transfer.Combination) error {
	return s.persistPartial(ctx, combination, []string{
		"LastUsageTime",
		"ValidationTime",
		"ValidatedMods",
		"ValidationProblems",
	})
}

// PersistExport will persist the export fields of the provided combination, excluding the export status.
func (s *Service) PersistExport(ctx context.Context, combination transfer.Combination) error {
	return s.persistPartial(ctx, combination, []string{
		"ExportError",
		"ExportTime",
		"ExportedMods",
		"ExportNumbers",
		"ExportFiles",
	})
}

// PersistExportStatus persists the provided export status to the combination. If the combination does not exists,
// no error will be returned.
func (s *Service) PersistExportStatus(
	ctx context.Context,
	combinationID transfer.CombinationID,
	status string,
	timestamp time.Time,
) error {
	combination := transfer.Combination{ID: combinationID}
	formattedTimestamp := timestamp.UTC().Truncate(time.Millisecond).Format(time.RFC3339Nano)

	update := expression.Set(
		expression.Name(fmt.Sprintf("ExportStatus.%s", status)),
		expression.Value(formattedTimestamp),
	)
	condition := expression.AttributeExists(expression.Name("ExportStatus"))
	expr, _ := expression.NewBuilder().WithUpdate(update).WithCondition(condition).Build()

	err := s.update(ctx, combination, expr)
	if err == nil {
		// No error means we successfully set the new status.
		return nil
	}

	var aerr smithy.APIError
	if !errors2.As(err, &aerr) || aerr.ErrorCode() != "ConditionalCheckFailedException" {
		// We did not get a ConditionalCheckFailedException, so it is some other kind of error.
		return errors.NewStorageError(err)
	}

	// We got a ConditionalCheckFailedException, so the combination did not have any ExportStatus map yet.
	// We have to add it as a new attribute.
	update = expression.Set(
		expression.Name("ExportStatus"),
		expression.Value(&types.AttributeValueMemberM{Value: map[string]types.AttributeValue{
			status: &types.AttributeValueMemberS{Value: formattedTimestamp},
		}}),
	)
	condition = expression.Equal(expression.Name("ID"), expression.Value(combinationID.String()))
	expr, _ = expression.NewBuilder().WithUpdate(update).WithCondition(condition).Build()

	err = s.update(ctx, combination, expr)
	if err == nil || (errors2.As(err, &aerr) && aerr.ErrorCode() == "ConditionalCheckFailedException") {
		// If the combination does not exist in the table, we again get a ConditionalCheckFailedException. We want to
		// ignore that one.
		return nil
	}
	return errors.NewStorageError(err)
}

// persistPartial will partially persist the provided combination by updating the provided fields.
func (s *Service) persistPartial(ctx context.Context, combination transfer.Combination, fields []string) error {
	item, _ := encoder.Encode(combination)
	values := item.(*types.AttributeValueMemberM).Value

	update := expression.UpdateBuilder{}
	for _, name := range fields {
		if value, ok := values[name]; ok {
			update = update.Set(expression.Name(name), expression.Value(value))
		}
	}
	expr, _ := expression.NewBuilder().WithUpdate(update).Build()

	err := s.update(ctx, combination, expr)
	if err != nil {
		return errors.NewStorageError(err)
	}
	return nil
}

// update will use the provided expression to update the combination. In case of an error, this method will return
// the unwrapped one.
func (s *Service) update(
	ctx context.Context,
	combination transfer.Combination,
	expr expression.Expression,
) error {
	input := dynamodb.UpdateItemInput{
		TableName: aws.String(s.tableName),
		Key: map[string]types.AttributeValue{
			"ID": &types.AttributeValueMemberS{Value: combination.ID.String()},
		},
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		UpdateExpression:          expr.Update(),
		ConditionExpression:       expr.Condition(),
	}

	_, err := s.table.UpdateItem(ctx, &input)
	return err
}

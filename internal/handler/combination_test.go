package handler

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/handler/mocks"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/request"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/response"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
)

func TestNewCombinationHandler(t *testing.T) {
	calculator := mocks.NewCalculator(t)
	combinationStorage := mocks.NewCombinationStorage(t)

	instance := NewCombinationHandler(calculator, combinationStorage)

	assert.NotNil(t, instance)
	assert.Same(t, calculator, instance.calculator)
	assert.Same(t, combinationStorage, instance.combinationStorage)
}

func TestCombinationHandler_HandleDetails(t *testing.T) {
	combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
	lastUsageTime := time.Date(2038, 1, 19, 3, 14, 7, 123000000, time.UTC)

	tests := map[string]struct {
		calculateResult           transfer.CombinationID
		calculateError            error
		expectFetch               bool
		fetchResult               *transfer.Combination
		fetchError                error
		expectUpdateLastUsageTime bool
		expectedErrorType         error
		expectedResponse          *response.CombinationDetails
	}{
		"happy path": {
			calculateResult: combinationID,
			calculateError:  nil,
			expectFetch:     true,
			fetchResult: &transfer.Combination{
				ID:            combinationID,
				ModNames:      []string{"base"},
				LastUsageTime: lastUsageTime,
			},
			fetchError:                nil,
			expectUpdateLastUsageTime: true,
			expectedErrorType:         nil,
			expectedResponse: &response.CombinationDetails{
				CombinationID:      "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
				ShortCombinationID: "1reA6H5z4uFpotvegbLIr4",
				ModNames:           []string{"base"},
			},
		},
		"missing combination": {
			calculateResult:           combinationID,
			calculateError:            nil,
			expectFetch:               true,
			fetchResult:               nil,
			fetchError:                nil,
			expectUpdateLastUsageTime: false,
			expectedErrorType:         &errors.CombinationNotKnownError{},
			expectedResponse:          nil,
		},
		"invalid combination id": {
			calculateResult:           transfer.CombinationID{},
			calculateError:            &errors.InvalidCombinationIDError{},
			expectFetch:               false,
			expectUpdateLastUsageTime: false,
			expectedErrorType:         &errors.InvalidCombinationIDError{},
			expectedResponse:          nil,
		},
		"fetch error": {
			calculateResult:           combinationID,
			calculateError:            nil,
			expectFetch:               true,
			fetchResult:               nil,
			fetchError:                &errors.StorageError{},
			expectUpdateLastUsageTime: false,
			expectedErrorType:         &errors.StorageError{},
			expectedResponse:          nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			req := request.CombinationIDRequest{
				CombinationID: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
			}

			calculator := mocks.NewCalculator(t)
			calculator.
				On("CalculateFromString", ctx, "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76").
				Once().
				Return(test.calculateResult, test.calculateError)

			combinationStorage := mocks.NewCombinationStorage(t)
			if test.expectFetch {
				combinationStorage.
					On("CombinationByID", ctx, test.calculateResult).
					Once().
					Return(test.fetchResult, test.fetchError)
			}
			if test.expectUpdateLastUsageTime {
				combinationStorage.On("PersistLastUsageTime", ctx, mock.MatchedBy(func(combination transfer.Combination) bool {
					return assert.Equal(t, *test.fetchResult, combination)
				})).Once()
			}

			instance := CombinationHandler{
				calculator:         calculator,
				combinationStorage: combinationStorage,
			}
			result, err := instance.HandleDetails(ctx, req)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResponse, result)
		})
	}
}

func TestCombinationHandler_HandleModNames(t *testing.T) {
	combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
	lastUsageTime := time.Date(2038, 1, 19, 3, 14, 7, 123000000, time.UTC)

	tests := map[string]struct {
		modNames                  []string
		expectCalculate           bool
		calculateResult           transfer.CombinationID
		calculateError            error
		expectFetch               bool
		fetchResult               *transfer.Combination
		fetchError                error
		expectedNewCombination    *transfer.Combination
		persistError              error
		expectUpdateLastUsageTime bool
		expectedErrorType         error
		expectedResponse          *response.CombinationDetails
	}{
		"existing combination": {
			modNames:        []string{"base"},
			expectCalculate: true,
			calculateResult: combinationID,
			calculateError:  nil,
			expectFetch:     true,
			fetchResult: &transfer.Combination{
				ID:            combinationID,
				ModNames:      []string{"base"},
				LastUsageTime: lastUsageTime,
			},
			fetchError:                nil,
			expectedNewCombination:    nil,
			persistError:              nil,
			expectUpdateLastUsageTime: true,
			expectedErrorType:         nil,
			expectedResponse: &response.CombinationDetails{
				CombinationID:      "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
				ShortCombinationID: "1reA6H5z4uFpotvegbLIr4",
				ModNames:           []string{"base"},
			},
		},
		"new combination": {
			modNames:        []string{"base"},
			expectCalculate: true,
			calculateResult: combinationID,
			calculateError:  nil,
			expectFetch:     true,
			fetchResult:     nil,
			fetchError:      nil,
			expectedNewCombination: &transfer.Combination{
				ID:            combinationID,
				ModNames:      []string{"base"},
				LastUsageTime: lastUsageTime,
			},
			persistError:              nil,
			expectUpdateLastUsageTime: false,
			expectedErrorType:         nil,
			expectedResponse: &response.CombinationDetails{
				CombinationID:      "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
				ShortCombinationID: "1reA6H5z4uFpotvegbLIr4",
				ModNames:           []string{"base"},
			},
		},
		"no mods": {
			modNames:                  []string{},
			expectCalculate:           false,
			expectFetch:               false,
			fetchResult:               nil,
			fetchError:                nil,
			expectedNewCombination:    nil,
			persistError:              nil,
			expectUpdateLastUsageTime: false,
			expectedErrorType:         &errors.InvalidRequestBodyError{},
			expectedResponse:          nil,
		},
		"calculate error": {
			modNames:                  []string{"base"},
			expectCalculate:           true,
			calculateResult:           transfer.CombinationID{},
			calculateError:            &errors.InvalidCombinationIDError{},
			expectFetch:               false,
			fetchResult:               nil,
			fetchError:                nil,
			expectedNewCombination:    nil,
			persistError:              nil,
			expectUpdateLastUsageTime: false,
			expectedErrorType:         &errors.InvalidCombinationIDError{},
			expectedResponse:          nil,
		},
		"fetch error": {
			modNames:                  []string{"base"},
			expectCalculate:           true,
			calculateResult:           combinationID,
			calculateError:            nil,
			expectFetch:               true,
			fetchResult:               nil,
			fetchError:                &errors.StorageError{},
			expectedNewCombination:    nil,
			persistError:              nil,
			expectUpdateLastUsageTime: false,
			expectedErrorType:         &errors.StorageError{},
			expectedResponse:          nil,
		},
		"persist error": {
			modNames:        []string{"base"},
			expectCalculate: true,
			calculateResult: combinationID,
			calculateError:  nil,
			expectFetch:     true,
			fetchResult:     nil,
			fetchError:      nil,
			expectedNewCombination: &transfer.Combination{
				ID:            combinationID,
				ModNames:      []string{"base"},
				LastUsageTime: lastUsageTime,
			},
			persistError:              &errors.StorageError{},
			expectUpdateLastUsageTime: false,
			expectedErrorType:         &errors.StorageError{},
			expectedResponse:          nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()

			calculator := mocks.NewCalculator(t)
			if test.expectCalculate {
				calculator.
					On("CalculateFromModNames", ctx, test.modNames).
					Once().
					Return(test.calculateResult, test.calculateError)
			}

			combinationStorage := mocks.NewCombinationStorage(t)
			if test.expectFetch {
				combinationStorage.
					On("CombinationByID", ctx, test.calculateResult).
					Once().
					Return(test.fetchResult, test.fetchError)
			}
			if test.expectedNewCombination != nil {
				combinationStorage.On("Persist", ctx, mock.MatchedBy(func(combination transfer.Combination) bool {
					combination.LastUsageTime = lastUsageTime
					return assert.Equal(t, *test.expectedNewCombination, combination)
				})).Once().Return(test.persistError)
			}
			if test.expectUpdateLastUsageTime {
				combinationStorage.On("PersistLastUsageTime", ctx, mock.MatchedBy(func(combination transfer.Combination) bool {
					return assert.Equal(t, *test.fetchResult, combination)
				})).Once()
			}

			instance := CombinationHandler{
				calculator:         calculator,
				combinationStorage: combinationStorage,
			}
			result, err := instance.HandleModNames(ctx, test.modNames)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResponse, result)
		})
	}
}

package handler

import (
	"context"
	"time"

	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/request"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/response"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

type factorioVersionFetcher interface {
	FactorioVersion(ctx context.Context) (modportalclient.Version, error)
}
type validator interface {
	Validate(ctx context.Context, modNames []string, factorioVersion modportalclient.Version) (map[string]modportalclient.Version, []transfer.ValidationProblem, error)
}

type ValidationHandler struct {
	calculator             calculator
	combinationStorage     combinationStorage
	factorioVersionFetcher factorioVersionFetcher
	validator              validator
}

func NewValidationHandler(
	calculator calculator,
	combinationStorage combinationStorage,
	factorioVersionFetcher factorioVersionFetcher,
	validator validator,
) *ValidationHandler {
	return &ValidationHandler{
		calculator:             calculator,
		combinationStorage:     combinationStorage,
		factorioVersionFetcher: factorioVersionFetcher,
		validator:              validator,
	}
}

func (h *ValidationHandler) HandleValidate(
	ctx context.Context,
	req request.CombinationIDRequest,
) (*response.CombinationDetails, error) {
	combinationID, err := h.calculator.CalculateFromString(ctx, req.CombinationID)
	if err != nil {
		return nil, err
	}

	combination, err := h.combinationStorage.CombinationByID(ctx, combinationID)
	if err != nil {
		return nil, err
	}
	if combination == nil {
		return nil, errors.NewCombinationNotKnownError(combinationID.String())
	}

	factorioVersion, err := h.factorioVersionFetcher.FactorioVersion(ctx)
	if err != nil {
		return nil, err
	}

	validatedMods, validationProblems, err := h.validator.Validate(ctx, combination.ModNames, factorioVersion)
	if err != nil {
		return nil, err
	}

	combination.LastUsageTime = time.Now().UTC().Truncate(1 * time.Millisecond)
	combination.ValidatedMods = validatedMods
	combination.ValidationProblems = validationProblems
	combination.ValidationTime = &combination.LastUsageTime
	err = h.combinationStorage.PersistValidation(ctx, *combination)
	if err != nil {
		return nil, err
	}

	return response.NewCombinationDetails(*combination), nil
}

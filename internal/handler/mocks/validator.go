// Code generated by mockery. DO NOT EDIT.

package mocks

import (
	context "context"

	mock "github.com/stretchr/testify/mock"

	modportalclient "gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"

	transfer "gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
)

// Validator is an autogenerated mock type for the validator type
type Validator struct {
	mock.Mock
}

// Validate provides a mock function with given fields: ctx, modNames, factorioVersion
func (_m *Validator) Validate(ctx context.Context, modNames []string, factorioVersion modportalclient.Version) (map[string]modportalclient.Version, []transfer.ValidationProblem, error) {
	ret := _m.Called(ctx, modNames, factorioVersion)

	if len(ret) == 0 {
		panic("no return value specified for Validate")
	}

	var r0 map[string]modportalclient.Version
	var r1 []transfer.ValidationProblem
	var r2 error
	if rf, ok := ret.Get(0).(func(context.Context, []string, modportalclient.Version) (map[string]modportalclient.Version, []transfer.ValidationProblem, error)); ok {
		return rf(ctx, modNames, factorioVersion)
	}
	if rf, ok := ret.Get(0).(func(context.Context, []string, modportalclient.Version) map[string]modportalclient.Version); ok {
		r0 = rf(ctx, modNames, factorioVersion)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(map[string]modportalclient.Version)
		}
	}

	if rf, ok := ret.Get(1).(func(context.Context, []string, modportalclient.Version) []transfer.ValidationProblem); ok {
		r1 = rf(ctx, modNames, factorioVersion)
	} else {
		if ret.Get(1) != nil {
			r1 = ret.Get(1).([]transfer.ValidationProblem)
		}
	}

	if rf, ok := ret.Get(2).(func(context.Context, []string, modportalclient.Version) error); ok {
		r2 = rf(ctx, modNames, factorioVersion)
	} else {
		r2 = ret.Error(2)
	}

	return r0, r1, r2
}

// NewValidator creates a new instance of Validator. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
// The first argument is typically a *testing.T value.
func NewValidator(t interface {
	mock.TestingT
	Cleanup(func())
}) *Validator {
	mock := &Validator{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}

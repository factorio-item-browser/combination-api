package handler

import (
	"context"

	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
)

type combinationStorage interface {
	Persist(ctx context.Context, combination transfer.Combination) error
	CombinationByID(ctx context.Context, combinationID transfer.CombinationID) (*transfer.Combination, error)
	PersistLastUsageTime(ctx context.Context, combination transfer.Combination)
	PersistValidation(ctx context.Context, combination transfer.Combination) error
}

type calculator interface {
	CalculateFromString(ctx context.Context, value string) (transfer.CombinationID, error)
	CalculateFromModNames(ctx context.Context, modNames []string) (transfer.CombinationID, error)
}

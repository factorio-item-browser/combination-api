package handler

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/event"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/handler/mocks"
)

func TestNewEventHandler(t *testing.T) {
	eventHandlers := []event.Handler{
		mocks.NewEventHandler(t),
		mocks.NewEventHandler(t),
	}

	instance := NewEventHandler(eventHandlers)

	assert.NotNil(t, instance)
	assert.Equal(t, eventHandlers, instance.eventHandlers)
}

type event1 struct {
	Foo string `json:"foo"`
}
type event2 struct {
	Foo int `json:"foo"`
}

func TestEventHandler_HandleCloudWatchEvent(t *testing.T) {
	ctx := context.Background()
	source := event.NewSource("foo.bar.baz.test")
	payload := `{"foo": "bar"}`
	cloudWatchEvent := events.CloudWatchEvent{
		Source: "foo.bar.baz.test",
		Detail: []byte(payload),
	}

	eventHandler1 := mocks.NewEventHandler(t)
	eventHandler1.On("Supports", ctx, source).Once().Return(&event1{}, true)
	eventHandler1.On("Handle", ctx, &event1{Foo: "bar"}).Once().Return(fmt.Errorf("test error"))

	eventHandler2 := mocks.NewEventHandler(t)
	eventHandler2.On("Supports", ctx, source).Once().Return(&event1{}, true)
	eventHandler2.On("Handle", ctx, &event1{Foo: "bar"}).Once().Return(nil)

	eventHandler3 := mocks.NewEventHandler(t)
	eventHandler3.On("Supports", ctx, source).Once().Return(nil, false)

	eventHandler4 := mocks.NewEventHandler(t)
	eventHandler4.On("Supports", ctx, source).Once().Return(&event2{}, true)

	instance := EventHandler{
		eventHandlers: []event.Handler{
			eventHandler1,
			eventHandler2,
			eventHandler3,
			eventHandler4,
		},
	}
	result, err := instance.HandleCloudWatchEvent(ctx, cloudWatchEvent)

	assert.Nil(t, err)
	assert.Nil(t, result)
}

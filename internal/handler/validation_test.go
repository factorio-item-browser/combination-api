package handler

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/handler/mocks"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/request"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/response"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

func TestNewValidationHandler(t *testing.T) {
	calculator := mocks.NewCalculator(t)
	combinationStorage := mocks.NewCombinationStorage(t)
	factorioVersionFetcher := mocks.NewFactorioVersionFetcher(t)
	validator := mocks.NewValidator(t)

	instance := NewValidationHandler(calculator, combinationStorage, factorioVersionFetcher, validator)

	assert.NotNil(t, instance)
	assert.Same(t, calculator, instance.calculator)
	assert.Same(t, combinationStorage, instance.combinationStorage)
	assert.Same(t, factorioVersionFetcher, instance.factorioVersionFetcher)
	assert.Same(t, validator, instance.validator)
}

func ptr[T any](t T) *T {
	return &t
}

func TestValidationHandler_HandleValidate(t *testing.T) {
	combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
	lastUsageTime := time.Date(2038, 1, 19, 3, 14, 7, 123000000, time.UTC)

	tests := map[string]struct {
		calculateResult       transfer.CombinationID
		calculateError        error
		expectFetch           bool
		fetchResult           *transfer.Combination
		fetchError            error
		expectFactorioVersion bool
		factorioVersionError  error
		expectValidate        bool
		validatedMods         map[string]modportalclient.Version
		validationProblems    []transfer.ValidationProblem
		validateError         error
		expectPersist         bool
		persistError          error
		expectedErrorType     error
		expectedResponse      *response.CombinationDetails
	}{
		"happy path": {
			calculateResult: combinationID,
			calculateError:  nil,
			expectFetch:     true,
			fetchResult: &transfer.Combination{
				ID:            combinationID,
				ModNames:      []string{"base"},
				LastUsageTime: lastUsageTime,
			},
			fetchError:            nil,
			expectFactorioVersion: true,
			factorioVersionError:  nil,
			expectValidate:        true,
			validatedMods:         map[string]modportalclient.Version{"base": modportalclient.NewVersion("1.2.3")},
			validationProblems: []transfer.ValidationProblem{
				{
					ModName: "foo",
					Type:    transfer.UnknownMod,
				},
			},
			validateError:     nil,
			expectPersist:     true,
			persistError:      nil,
			expectedErrorType: nil,
			expectedResponse: &response.CombinationDetails{
				CombinationID:      "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
				ShortCombinationID: "1reA6H5z4uFpotvegbLIr4",
				ModNames:           []string{"base"},
				Validation: &response.Validation{
					IsValid: false,
					ValidatedMods: []response.ValidatedMod{
						{
							Name:     "base",
							Version:  ptr(modportalclient.NewVersion("1.2.3")),
							Problems: []response.ValidationProblem{},
						},
						{
							Name: "foo",
							Problems: []response.ValidationProblem{
								{
									Type: transfer.UnknownMod,
								},
							},
						},
					},
					ValidationTime: lastUsageTime,
				},
			},
		},
		"unknown combination": {
			calculateResult:       combinationID,
			calculateError:        nil,
			expectFetch:           true,
			fetchResult:           nil,
			fetchError:            nil,
			expectFactorioVersion: false,
			factorioVersionError:  nil,
			expectValidate:        false,
			validatedMods:         nil,
			validationProblems:    nil,
			validateError:         nil,
			expectPersist:         false,
			persistError:          nil,
			expectedErrorType:     &errors.CombinationNotKnownError{},
			expectedResponse:      nil,
		},
		"invalid combination id": {
			calculateResult:       transfer.CombinationID{},
			calculateError:        &errors.InvalidCombinationIDError{},
			expectFetch:           false,
			fetchResult:           nil,
			fetchError:            nil,
			expectFactorioVersion: false,
			factorioVersionError:  nil,
			expectValidate:        false,
			validatedMods:         nil,
			validationProblems:    nil,
			validateError:         nil,
			expectPersist:         false,
			persistError:          nil,
			expectedErrorType:     &errors.InvalidCombinationIDError{},
			expectedResponse:      nil,
		},
		"fetch error": {
			calculateResult:       combinationID,
			calculateError:        nil,
			expectFetch:           true,
			fetchResult:           nil,
			fetchError:            &errors.StorageError{},
			expectFactorioVersion: false,
			factorioVersionError:  nil,
			expectValidate:        false,
			validatedMods:         nil,
			validationProblems:    nil,
			validateError:         nil,
			expectPersist:         false,
			persistError:          nil,
			expectedErrorType:     &errors.StorageError{},
			expectedResponse:      nil,
		},
		"factorio version error": {
			calculateResult: combinationID,
			calculateError:  nil,
			expectFetch:     true,
			fetchResult: &transfer.Combination{
				ID:            combinationID,
				ModNames:      []string{"base"},
				LastUsageTime: lastUsageTime,
			},
			fetchError:            nil,
			expectFactorioVersion: true,
			factorioVersionError:  fmt.Errorf("test error"),
			expectValidate:        false,
			validatedMods:         nil,
			validationProblems:    nil,
			validateError:         nil,
			expectPersist:         false,
			persistError:          nil,
			expectedErrorType:     fmt.Errorf("test error"),
			expectedResponse:      nil,
		},
		"validate error": {
			calculateResult: combinationID,
			calculateError:  nil,
			expectFetch:     true,
			fetchResult: &transfer.Combination{
				ID:            combinationID,
				ModNames:      []string{"base"},
				LastUsageTime: lastUsageTime,
			},
			fetchError:            nil,
			expectFactorioVersion: true,
			factorioVersionError:  nil,
			expectValidate:        true,
			validatedMods:         nil,
			validationProblems:    nil,
			validateError:         &errors.StorageError{},
			expectPersist:         false,
			persistError:          nil,
			expectedErrorType:     &errors.StorageError{},
			expectedResponse:      nil,
		},
		"persist error": {
			calculateResult: combinationID,
			calculateError:  nil,
			expectFetch:     true,
			fetchResult: &transfer.Combination{
				ID:            combinationID,
				ModNames:      []string{"base"},
				LastUsageTime: lastUsageTime,
			},
			fetchError:            nil,
			expectFactorioVersion: true,
			factorioVersionError:  nil,
			expectValidate:        true,
			validatedMods:         map[string]modportalclient.Version{"base": modportalclient.NewVersion("1.2.3")},
			validationProblems: []transfer.ValidationProblem{
				{
					ModName: "foo",
					Type:    transfer.UnknownMod,
				},
			},
			validateError:     nil,
			expectPersist:     true,
			persistError:      &errors.StorageError{},
			expectedErrorType: &errors.StorageError{},
			expectedResponse:  nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			factorioVersion := modportalclient.NewVersion("1.2.3")
			req := request.CombinationIDRequest{
				CombinationID: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
			}

			calculator := mocks.NewCalculator(t)
			calculator.
				On("CalculateFromString", ctx, "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76").
				Once().
				Return(test.calculateResult, test.calculateError)

			combinationStorage := mocks.NewCombinationStorage(t)
			if test.expectFetch {
				combinationStorage.
					On("CombinationByID", ctx, test.calculateResult).
					Once().
					Return(test.fetchResult, test.fetchError)
			}
			if test.expectPersist {
				expectedCombination := *test.fetchResult
				expectedCombination.ValidatedMods = test.validatedMods
				expectedCombination.ValidationProblems = test.validationProblems
				expectedCombination.LastUsageTime = lastUsageTime
				expectedCombination.ValidationTime = &lastUsageTime

				combinationStorage.On("PersistValidation", ctx, mock.MatchedBy(func(combination transfer.Combination) bool {
					assert.Equal(t, &combination.LastUsageTime, combination.ValidationTime)

					combination.LastUsageTime = lastUsageTime
					combination.ValidationTime = &lastUsageTime
					return assert.Equal(t, expectedCombination, combination)
				})).Once().Return(test.persistError)
			}

			factorioVersionFetcher := mocks.NewFactorioVersionFetcher(t)
			if test.expectFactorioVersion {
				factorioVersionFetcher.On("FactorioVersion", ctx).Once().Return(factorioVersion, test.factorioVersionError)
			}

			validator := mocks.NewValidator(t)
			if test.expectValidate {
				validator.
					On("Validate", ctx, test.fetchResult.ModNames, factorioVersion).
					Once().
					Return(test.validatedMods, test.validationProblems, test.validateError)
			}

			instance := ValidationHandler{
				calculator:             calculator,
				combinationStorage:     combinationStorage,
				factorioVersionFetcher: factorioVersionFetcher,
				validator:              validator,
			}
			result, err := instance.HandleValidate(ctx, req)

			assert.IsType(t, test.expectedErrorType, err)

			if test.expectedResponse != nil {
				result.Validation.ValidationTime = lastUsageTime

				assert.Equal(t, test.expectedResponse, result)
			}
		})
	}
}

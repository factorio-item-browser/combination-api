package handler

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/request"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/response"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
)

type CombinationHandler struct {
	calculator         calculator
	combinationStorage combinationStorage
}

func NewCombinationHandler(calculator calculator, combinationStorage combinationStorage) *CombinationHandler {
	return &CombinationHandler{
		calculator:         calculator,
		combinationStorage: combinationStorage,
	}
}

func (h *CombinationHandler) HandleDetails(
	ctx context.Context,
	req request.CombinationIDRequest,
) (*response.CombinationDetails, error) {
	combinationID, err := h.calculator.CalculateFromString(ctx, req.CombinationID)
	if err != nil {
		return nil, err
	}

	combination, err := h.combinationStorage.CombinationByID(ctx, combinationID)
	if err != nil {
		return nil, err
	}
	if combination == nil {
		return nil, errors.NewCombinationNotKnownError(combinationID.String())
	}

	combination.LastUsageTime = time.Now().UTC().Truncate(1 * time.Millisecond)
	h.combinationStorage.PersistLastUsageTime(ctx, *combination)

	return response.NewCombinationDetails(*combination), nil
}

func (h *CombinationHandler) HandleModNames(
	ctx context.Context,
	modNames []string,
) (*response.CombinationDetails, error) {
	if len(modNames) == 0 {
		return nil, errors.NewInvalidRequestBodyError(fmt.Errorf("no mod names provided"))
	}

	combinationID, err := h.calculator.CalculateFromModNames(ctx, modNames)
	if err != nil {
		return nil, err
	}

	combination, err := h.combinationStorage.CombinationByID(ctx, combinationID)
	if err != nil {
		return nil, err
	}
	if combination == nil {
		combination = &transfer.Combination{
			ID:            combinationID,
			ModNames:      modNames,
			LastUsageTime: time.Now(),
		}

		err = h.combinationStorage.Persist(ctx, *combination)
		if err != nil {
			return nil, err
		}
	} else {
		combination.LastUsageTime = time.Now()
		h.combinationStorage.PersistLastUsageTime(ctx, *combination)
	}

	return response.NewCombinationDetails(*combination), nil
}

package handler

import (
	"context"
	"encoding/json"
	"log/slog"

	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/event"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/log"
)

type EventHandler struct {
	eventHandlers []event.Handler
}

func NewEventHandler(eventHandlers []event.Handler) *EventHandler {
	return &EventHandler{
		eventHandlers: eventHandlers,
	}
}

func (h *EventHandler) HandleCloudWatchEvent(ctx context.Context, cloudWatchEvent events.CloudWatchEvent) (any, error) {
	logger := log.FromContext(ctx)
	logger.Info(
		log.MessageIncomingEvent,
		slog.String("source", cloudWatchEvent.Source),
		slog.String("payload", string(cloudWatchEvent.Detail)),
	)

	source := event.NewSource(cloudWatchEvent.Source)
	for _, eventHandler := range h.eventHandlers {
		payload, ok := eventHandler.Supports(ctx, source)
		if !ok {
			continue
		}

		err := json.Unmarshal(cloudWatchEvent.Detail, &payload)
		if err != nil {
			logger.Error(log.MessageJSONUnmarshalError, slog.String("error", err.Error()))
			continue
		}

		err = eventHandler.Handle(ctx, payload)
		if err != nil {
			logger.Error(log.MessageEventHandleError, slog.String("error", err.Error()))
		}
	}

	return nil, nil
}

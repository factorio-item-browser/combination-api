package event

import (
	"context"
	"time"

	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
)

type ErrorEvent struct {
	CombinationID string `json:"combinationID"`
	ErrorType     string `json:"errorType"`
	ErrorMessage  string `json:"errorMessage"`
	Timestamp     string `json:"timestamp"`
}

type ErrorHandler struct {
	calculator      calculator
	exportPersister exportPersister
}

func NewErrorHandler(calculator calculator, exportPersister exportPersister) *ErrorHandler {
	return &ErrorHandler{
		calculator:      calculator,
		exportPersister: exportPersister,
	}
}

func (h *ErrorHandler) Supports(_ context.Context, source Source) (any, bool) {
	if source.Project != "factorio-item-browser" || source.Component != "export" || source.Type != "error" {
		return nil, false
	}

	return &ErrorEvent{}, true
}

func (h *ErrorHandler) Handle(ctx context.Context, payload any) error {
	event := payload.(*ErrorEvent)

	combinationID, err := h.calculator.CalculateFromString(ctx, event.CombinationID)
	if err != nil {
		return err
	}

	timestamp, err := time.Parse(time.RFC3339Nano, event.Timestamp)
	if err != nil {
		return err
	}
	timestamp = timestamp.UTC().Truncate(1 * time.Millisecond)

	err = h.exportPersister.PersistExportStatus(ctx, combinationID, "error", timestamp)
	if err != nil {
		return err
	}

	combination := transfer.Combination{
		ID: combinationID,
		ExportError: &transfer.ExportError{
			Type:    event.ErrorType,
			Message: event.ErrorMessage,
		},
	}
	return h.exportPersister.PersistExport(ctx, combination)
}

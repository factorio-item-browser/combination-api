package event

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/event/mocks"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

func TestNewSuccessHandler(t *testing.T) {
	calculator := mocks.NewCalculator(t)
	exportPersister := mocks.NewExportPersister(t)

	instance := NewSuccessHandler(calculator, exportPersister)

	assert.NotNil(t, instance)
	assert.Same(t, calculator, instance.calculator)
	assert.Same(t, exportPersister, instance.exportPersister)
}

func TestSuccessHandler_Supports(t *testing.T) {
	tests := map[string]struct {
		source         Source
		expectedResult any
		expectedOK     bool
	}{
		"happy path": {
			source: Source{
				Project:     "factorio-item-browser",
				Component:   "export",
				Type:        "success",
				Environment: "test",
			},
			expectedResult: &SuccessEvent{},
			expectedOK:     true,
		},
		"wrong project": {
			source: Source{
				Project:     "foo",
				Component:   "export",
				Type:        "success",
				Environment: "test",
			},
			expectedResult: nil,
			expectedOK:     false,
		},
		"wrong component": {
			source: Source{
				Project:     "factorio-item-browser",
				Component:   "foo",
				Type:        "success",
				Environment: "test",
			},
			expectedResult: nil,
			expectedOK:     false,
		},
		"wrong type": {
			source: Source{
				Project:     "factorio-item-browser",
				Component:   "export",
				Type:        "foo",
				Environment: "test",
			},
			expectedResult: nil,
			expectedOK:     false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()

			instance := SuccessHandler{}
			result, ok := instance.Supports(ctx, test.source)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedOK, ok)
		})
	}
}

func TestSuccessHandler_Handle(t *testing.T) {
	combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
	payload := SuccessEvent{
		CombinationID: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
		Timestamp:     "2038-01-19T03:14:07.123456Z",
		Mods: map[string]string{
			"foo": "1.2.3",
			"bar": "4.5.6",
		},
		Numbers: map[string]uint64{
			"thing": 42,
			"thong": 21,
		},
		Files: map[string]SuccessEventFile{
			"abc": {
				Size:           7331,
				CompressedSize: 1337,
			},
			"def": {
				Size:           654,
				CompressedSize: 321,
			},
		},
	}

	tests := map[string]struct {
		payload             any
		calculateResult     transfer.CombinationID
		calculateError      error
		expectPersistStatus bool
		persistStatusError  error
		expectPersist       bool
		persistError        error
		expectedErrorType   error
	}{
		"happy path": {
			payload:             &payload,
			calculateResult:     combinationID,
			calculateError:      nil,
			expectPersistStatus: true,
			persistStatusError:  nil,
			expectPersist:       true,
			persistError:        nil,
			expectedErrorType:   nil,
		},
		"invalid combination id": {
			payload:             &payload,
			calculateResult:     transfer.CombinationID{},
			calculateError:      &errors.InvalidCombinationIDError{},
			expectPersistStatus: false,
			persistStatusError:  nil,
			expectPersist:       false,
			persistError:        nil,
			expectedErrorType:   &errors.InvalidCombinationIDError{},
		},
		"invalid timestamp": {
			payload: &SuccessEvent{
				CombinationID: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
				Timestamp:     "invalid",
			},
			calculateResult:     combinationID,
			calculateError:      nil,
			expectPersistStatus: false,
			persistStatusError:  nil,
			expectPersist:       false,
			persistError:        nil,
			expectedErrorType:   &time.ParseError{},
		},
		"export status error": {
			payload:             &payload,
			calculateResult:     combinationID,
			calculateError:      nil,
			expectPersistStatus: true,
			persistStatusError:  &errors.StorageError{},
			expectPersist:       false,
			persistError:        nil,
			expectedErrorType:   &errors.StorageError{},
		},
		"export error": {
			payload:             &payload,
			calculateResult:     combinationID,
			calculateError:      nil,
			expectPersistStatus: true,
			persistStatusError:  nil,
			expectPersist:       true,
			persistError:        &errors.StorageError{},
			expectedErrorType:   &errors.StorageError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			timestamp := time.Date(2038, 1, 19, 3, 14, 7, 123000000, time.UTC)

			expectedCombination := transfer.Combination{
				ID:         combinationID,
				ExportTime: &timestamp,
				ExportedMods: map[string]modportalclient.Version{
					"foo": modportalclient.NewVersion("1.2.3"),
					"bar": modportalclient.NewVersion("4.5.6"),
				},
				ExportNumbers: map[string]uint64{
					"thing": 42,
					"thong": 21,
				},
				ExportFiles: map[string]transfer.ExportFile{
					"abc": {
						Size:           7331,
						CompressedSize: 1337,
					},
					"def": {
						Size:           654,
						CompressedSize: 321,
					},
				},
			}

			calculator := mocks.NewCalculator(t)
			calculator.
				On("CalculateFromString", ctx, "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76").
				Once().
				Return(test.calculateResult, test.calculateError)

			exportPersister := mocks.NewExportPersister(t)
			if test.expectPersistStatus {
				exportPersister.
					On("PersistExportStatus", ctx, combinationID, "success", timestamp).
					Once().
					Return(test.persistStatusError)
			}
			if test.expectPersist {
				exportPersister.On("PersistExport", ctx, expectedCombination).Once().Return(test.persistError)
			}

			instance := SuccessHandler{
				calculator:      calculator,
				exportPersister: exportPersister,
			}
			err := instance.Handle(ctx, test.payload)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}

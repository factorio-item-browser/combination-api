package event

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/event/mocks"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
)

func TestNewErrorHandler(t *testing.T) {
	calculator := mocks.NewCalculator(t)
	exportPersister := mocks.NewExportPersister(t)

	instance := NewErrorHandler(calculator, exportPersister)

	assert.NotNil(t, instance)
	assert.Same(t, calculator, instance.calculator)
	assert.Same(t, exportPersister, instance.exportPersister)
}

func TestErrorHandler_Supports(t *testing.T) {
	tests := map[string]struct {
		source         Source
		expectedResult any
		expectedOK     bool
	}{
		"happy path": {
			source: Source{
				Project:     "factorio-item-browser",
				Component:   "export",
				Type:        "error",
				Environment: "test",
			},
			expectedResult: &ErrorEvent{},
			expectedOK:     true,
		},
		"wrong project": {
			source: Source{
				Project:     "foo",
				Component:   "export",
				Type:        "error",
				Environment: "test",
			},
			expectedResult: nil,
			expectedOK:     false,
		},
		"wrong component": {
			source: Source{
				Project:     "factorio-item-browser",
				Component:   "foo",
				Type:        "error",
				Environment: "test",
			},
			expectedResult: nil,
			expectedOK:     false,
		},
		"wrong type": {
			source: Source{
				Project:     "factorio-item-browser",
				Component:   "export",
				Type:        "foo",
				Environment: "test",
			},
			expectedResult: nil,
			expectedOK:     false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()

			instance := ErrorHandler{}
			result, ok := instance.Supports(ctx, test.source)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedOK, ok)
		})
	}
}

func TestErrorHandler_Handle(t *testing.T) {
	combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
	payload := ErrorEvent{
		CombinationID: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
		ErrorType:     "test type",
		ErrorMessage:  "test message",
		Timestamp:     "2038-01-19T03:14:07.123456Z",
	}

	tests := map[string]struct {
		payload             any
		calculateResult     transfer.CombinationID
		calculateError      error
		expectPersistStatus bool
		persistStatusError  error
		expectPersist       bool
		persistError        error
		expectedErrorType   error
	}{
		"happy path": {
			payload:             &payload,
			calculateResult:     combinationID,
			calculateError:      nil,
			expectPersistStatus: true,
			persistStatusError:  nil,
			expectPersist:       true,
			persistError:        nil,
			expectedErrorType:   nil,
		},
		"invalid combination id": {
			payload:             &payload,
			calculateResult:     transfer.CombinationID{},
			calculateError:      &errors.InvalidCombinationIDError{},
			expectPersistStatus: false,
			persistStatusError:  nil,
			expectPersist:       false,
			persistError:        nil,
			expectedErrorType:   &errors.InvalidCombinationIDError{},
		},
		"invalid timestamp": {
			payload: &ErrorEvent{
				CombinationID: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
				ErrorType:     "test type",
				ErrorMessage:  "test message",
				Timestamp:     "invalid",
			},
			calculateResult:     combinationID,
			calculateError:      nil,
			expectPersistStatus: false,
			persistStatusError:  nil,
			expectPersist:       false,
			persistError:        nil,
			expectedErrorType:   &time.ParseError{},
		},
		"export status error": {
			payload:             &payload,
			calculateResult:     combinationID,
			calculateError:      nil,
			expectPersistStatus: true,
			persistStatusError:  &errors.StorageError{},
			expectPersist:       false,
			persistError:        nil,
			expectedErrorType:   &errors.StorageError{},
		},
		"export error": {
			payload:             &payload,
			calculateResult:     combinationID,
			calculateError:      nil,
			expectPersistStatus: true,
			persistStatusError:  nil,
			expectPersist:       true,
			persistError:        &errors.StorageError{},
			expectedErrorType:   &errors.StorageError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			timestamp := time.Date(2038, 1, 19, 3, 14, 7, 123000000, time.UTC)

			expectedCombination := transfer.Combination{
				ID: combinationID,
				ExportError: &transfer.ExportError{
					Type:    "test type",
					Message: "test message",
				},
			}

			calculator := mocks.NewCalculator(t)
			calculator.
				On("CalculateFromString", ctx, "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76").
				Once().
				Return(test.calculateResult, test.calculateError)

			exportPersister := mocks.NewExportPersister(t)
			if test.expectPersistStatus {
				exportPersister.
					On("PersistExportStatus", ctx, combinationID, "error", timestamp).
					Once().
					Return(test.persistStatusError)
			}
			if test.expectPersist {
				exportPersister.On("PersistExport", ctx, expectedCombination).Once().Return(test.persistError)
			}

			instance := ErrorHandler{
				calculator:      calculator,
				exportPersister: exportPersister,
			}
			err := instance.Handle(ctx, test.payload)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}

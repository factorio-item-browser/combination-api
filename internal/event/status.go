package event

import (
	"context"
	"time"
)

type StatusEvent struct {
	CombinationID string `json:"combinationID"`
	Status        string `json:"status"`
	Timestamp     string `json:"timestamp"`
}

type StatusHandler struct {
	calculator            calculator
	exportStatusPersister exportStatusPersister
}

func NewStatusHandler(calculator calculator, exportStatusPersister exportStatusPersister) *StatusHandler {
	return &StatusHandler{
		calculator:            calculator,
		exportStatusPersister: exportStatusPersister,
	}
}

func (h *StatusHandler) Supports(_ context.Context, source Source) (any, bool) {
	if source.Project != "factorio-item-browser" || source.Component != "export" || source.Type != "status" {
		return nil, false
	}

	return &StatusEvent{}, true
}

func (h *StatusHandler) Handle(ctx context.Context, payload any) error {
	event := payload.(*StatusEvent)

	combinationID, err := h.calculator.CalculateFromString(ctx, event.CombinationID)
	if err != nil {
		return err
	}

	timestamp, err := time.Parse(time.RFC3339Nano, event.Timestamp)
	if err != nil {
		return err
	}
	timestamp = timestamp.UTC().Truncate(1 * time.Millisecond)

	return h.exportStatusPersister.PersistExportStatus(ctx, combinationID, event.Status, timestamp)
}

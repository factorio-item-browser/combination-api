package event

import (
	"context"
	"time"

	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

type SuccessEvent struct {
	CombinationID string                      `json:"combinationID"`
	Timestamp     string                      `json:"timestamp"`
	Mods          map[string]string           `json:"mods"`
	Numbers       map[string]uint64           `json:"numbers"`
	Files         map[string]SuccessEventFile `json:"files"`
}

type SuccessEventFile struct {
	Size           uint64 `json:"size"`
	CompressedSize uint64 `json:"compressedSize"`
}

type SuccessHandler struct {
	calculator      calculator
	exportPersister exportPersister
}

func NewSuccessHandler(calculator calculator, exportPersister exportPersister) *SuccessHandler {
	return &SuccessHandler{
		calculator:      calculator,
		exportPersister: exportPersister,
	}
}

func (h *SuccessHandler) Supports(_ context.Context, source Source) (any, bool) {
	if source.Project != "factorio-item-browser" || source.Component != "export" || source.Type != "success" {
		return nil, false
	}

	return &SuccessEvent{}, true
}

func (h *SuccessHandler) Handle(ctx context.Context, payload any) error {
	event := payload.(*SuccessEvent)

	combinationID, err := h.calculator.CalculateFromString(ctx, event.CombinationID)
	if err != nil {
		return err
	}

	timestamp, err := time.Parse(time.RFC3339Nano, event.Timestamp)
	if err != nil {
		return err
	}
	timestamp = timestamp.UTC().Truncate(1 * time.Millisecond)

	err = h.exportPersister.PersistExportStatus(ctx, combinationID, "success", timestamp)
	if err != nil {
		return err
	}

	combination := transfer.Combination{
		ID:            combinationID,
		ExportTime:    &timestamp,
		ExportedMods:  make(map[string]modportalclient.Version, len(event.Mods)),
		ExportNumbers: event.Numbers,
		ExportFiles:   make(map[string]transfer.ExportFile, len(event.Files)),
	}
	for modName, version := range event.Mods {
		combination.ExportedMods[modName] = modportalclient.NewVersion(version)
	}
	for name, file := range event.Files {
		combination.ExportFiles[name] = transfer.ExportFile{
			Size:           file.Size,
			CompressedSize: file.CompressedSize,
		}
	}

	return h.exportPersister.PersistExport(ctx, combination)
}

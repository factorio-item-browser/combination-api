package event

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewSource(t *testing.T) {
	tests := map[string]struct {
		source         string
		expectedResult Source
	}{
		"happy path": {
			source: "factorio-item-browser.export.status.test",
			expectedResult: Source{
				Project:     "factorio-item-browser",
				Component:   "export",
				Type:        "status",
				Environment: "test",
			},
		},
		"empty source": {
			source:         "",
			expectedResult: Source{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			result := NewSource(test.source)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

package event

import (
	"context"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/event/mocks"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
)

func TestNewStatusHandler(t *testing.T) {
	calculator := mocks.NewCalculator(t)
	exportStatusPersister := mocks.NewExportStatusPersister(t)

	instance := NewStatusHandler(calculator, exportStatusPersister)

	assert.NotNil(t, instance)
	assert.Same(t, calculator, instance.calculator)
	assert.Same(t, exportStatusPersister, instance.exportStatusPersister)
}

func TestStatusHandler_Supports(t *testing.T) {
	tests := map[string]struct {
		source         Source
		expectedResult any
		expectedOK     bool
	}{
		"happy path": {
			source: Source{
				Project:     "factorio-item-browser",
				Component:   "export",
				Type:        "status",
				Environment: "test",
			},
			expectedResult: &StatusEvent{},
			expectedOK:     true,
		},
		"wrong project": {
			source: Source{
				Project:     "foo",
				Component:   "export",
				Type:        "status",
				Environment: "test",
			},
			expectedResult: nil,
			expectedOK:     false,
		},
		"wrong component": {
			source: Source{
				Project:     "factorio-item-browser",
				Component:   "foo",
				Type:        "status",
				Environment: "test",
			},
			expectedResult: nil,
			expectedOK:     false,
		},
		"wrong type": {
			source: Source{
				Project:     "factorio-item-browser",
				Component:   "export",
				Type:        "foo",
				Environment: "test",
			},
			expectedResult: nil,
			expectedOK:     false,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()

			instance := StatusHandler{}
			result, ok := instance.Supports(ctx, test.source)

			assert.Equal(t, test.expectedResult, result)
			assert.Equal(t, test.expectedOK, ok)
		})
	}
}

func TestStatusHandler_Handle(t *testing.T) {
	combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))
	payload := StatusEvent{
		CombinationID: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
		Status:        "status",
		Timestamp:     "2038-01-19T03:14:07.123456Z",
	}

	tests := map[string]struct {
		payload           any
		calculateResult   transfer.CombinationID
		calculateError    error
		expectPersist     bool
		persistError      error
		expectedErrorType error
	}{
		"happy path": {
			payload:           &payload,
			calculateResult:   combinationID,
			calculateError:    nil,
			expectPersist:     true,
			persistError:      nil,
			expectedErrorType: nil,
		},
		"invalid combination id": {
			payload:           &payload,
			calculateResult:   transfer.CombinationID{},
			calculateError:    &errors.InvalidCombinationIDError{},
			expectPersist:     false,
			persistError:      nil,
			expectedErrorType: &errors.InvalidCombinationIDError{},
		},
		"invalid timestamp": {
			payload: &StatusEvent{
				CombinationID: "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
				Status:        "status",
				Timestamp:     "invalid",
			},
			calculateResult:   combinationID,
			calculateError:    nil,
			expectPersist:     false,
			persistError:      nil,
			expectedErrorType: &time.ParseError{},
		},
		"persist error": {
			payload:           &payload,
			calculateResult:   combinationID,
			calculateError:    nil,
			expectPersist:     true,
			persistError:      &errors.StorageError{},
			expectedErrorType: &errors.StorageError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			expectedTimestamp := time.Date(2038, 1, 19, 3, 14, 7, 123000000, time.UTC)

			calculator := mocks.NewCalculator(t)
			calculator.
				On("CalculateFromString", ctx, "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76").
				Once().
				Return(test.calculateResult, test.calculateError)

			exportStatusPersister := mocks.NewExportStatusPersister(t)
			if test.expectPersist {
				exportStatusPersister.
					On("PersistExportStatus", ctx, test.calculateResult, "status", expectedTimestamp).
					Once().
					Return(test.persistError)
			}

			instance := StatusHandler{
				calculator:            calculator,
				exportStatusPersister: exportStatusPersister,
			}
			err := instance.Handle(ctx, test.payload)

			assert.IsType(t, test.expectedErrorType, err)
		})
	}
}

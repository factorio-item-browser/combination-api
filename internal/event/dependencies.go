package event

import (
	"context"
	"time"

	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
)

type Handler interface {
	Supports(ctx context.Context, source Source) (any, bool)
	Handle(ctx context.Context, payload any) error
}

type calculator interface {
	CalculateFromString(ctx context.Context, value string) (transfer.CombinationID, error)
}

type exportStatusPersister interface {
	PersistExportStatus(ctx context.Context, combinationID transfer.CombinationID, status string, timestamp time.Time) error
}

type exportPersister interface {
	exportStatusPersister

	PersistExport(ctx context.Context, combination transfer.Combination) error
}

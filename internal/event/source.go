package event

import "strings"

type Source struct {
	Project     string
	Component   string
	Type        string
	Environment string
}

func NewSource(source string) Source {
	parts := append(strings.Split(source, "."), "", "", "", "")

	return Source{
		Project:     parts[0],
		Component:   parts[1],
		Type:        parts[2],
		Environment: parts[3],
	}
}

package request

import (
	"context"
	"encoding/json"

	"github.com/aws/aws-lambda-go/events"
	"github.com/go-viper/mapstructure/v2"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
)

// NewTransformProxyMiddleware creates a new middleware which will read the passed in APIGatewayProxyRequest,
// and create the actual request from it, which is then passed to the provided handler.
// When available, the request body will be unmarshalled as JSON.
func NewTransformProxyMiddleware[Request any, Response any](
	handler func(context.Context, Request) (Response, error),
) func(context.Context, events.APIGatewayProxyRequest) (Response, error) {
	return func(ctx context.Context, proxyRequest events.APIGatewayProxyRequest) (Response, error) {
		var request Request

		if proxyRequest.Body != "" {
			err := json.Unmarshal([]byte(proxyRequest.Body), &request)
			if err != nil {
				var zero Response
				return zero, errors.NewInvalidRequestBodyError(err)
			}
		}

		if len(proxyRequest.PathParameters) > 0 {
			decoder, _ := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
				Result:               &request,
				TagName:              "path",
				IgnoreUntaggedFields: true,
			})
			_ = decoder.Decode(proxyRequest.PathParameters)
		}

		return handler(ctx, request)
	}
}

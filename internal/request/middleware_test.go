package request

import (
	"context"
	"fmt"
	"testing"

	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
)

type testRequest struct {
	Foo string `path:"foo"`
	Bar string `json:"bar"`
}

func TestNewTransformProxyMiddleware(t *testing.T) {
	tests := map[string]struct {
		proxyRequest      events.APIGatewayProxyRequest
		expectHandler     bool
		expectedRequest   testRequest
		handlerError      error
		expectedErrorType error
	}{
		"bath and body": {
			proxyRequest: events.APIGatewayProxyRequest{
				PathParameters: map[string]string{
					"foo": "bar",
				},
				Body: `{"foo":"oof","bar":"rab"}`,
			},
			expectHandler: true,
			expectedRequest: testRequest{
				Foo: "bar",
				Bar: "rab",
			},
			handlerError:      fmt.Errorf("test error"),
			expectedErrorType: fmt.Errorf("test error"),
		},
		"without body": {
			proxyRequest: events.APIGatewayProxyRequest{
				PathParameters: map[string]string{
					"foo": "bar",
				},
			},
			expectHandler: true,
			expectedRequest: testRequest{
				Foo: "bar",
			},
			handlerError:      fmt.Errorf("test error"),
			expectedErrorType: fmt.Errorf("test error"),
		},
		"without path": {
			proxyRequest: events.APIGatewayProxyRequest{
				Body: `{"bar":"foo"}`,
			},
			expectHandler: true,
			expectedRequest: testRequest{
				Bar: "foo",
			},
			handlerError:      fmt.Errorf("test error"),
			expectedErrorType: fmt.Errorf("test error"),
		},
		"invalid body": {
			proxyRequest: events.APIGatewayProxyRequest{
				Body: `{"invalid`,
			},
			expectHandler:     false,
			expectedRequest:   testRequest{},
			handlerError:      nil,
			expectedErrorType: &errors.InvalidRequestBodyError{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			handlerResponse := "fancy response"

			handler := func(c context.Context, req testRequest) (any, error) {
				if !test.expectHandler {
					assert.Fail(t, "handler was not expected to be called")
				} else {
					assert.Equal(t, ctx, c)
					assert.Equal(t, test.expectedRequest, req)
				}

				return handlerResponse, test.handlerError
			}

			instance := NewTransformProxyMiddleware(handler)
			result, err := instance(ctx, test.proxyRequest)

			assert.IsType(t, test.expectedErrorType, err)

			if test.expectHandler {
				assert.Equal(t, handlerResponse, result)
			}
		})
	}
}

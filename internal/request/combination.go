package request

type CombinationIDRequest struct {
	CombinationID string `path:"combinationID"`
}

package response

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

func ptr[T any](t T) *T {
	return &t
}

func TestNewCombinationDetails(t *testing.T) {
	combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))

	tests := map[string]struct {
		combination    transfer.Combination
		expectedResult *CombinationDetails
	}{
		"minimal": {
			combination: transfer.Combination{
				ID:            combinationID,
				ModNames:      []string{"base"},
				LastUsageTime: time.Date(2038, 1, 19, 3, 14, 7, 0, time.UTC),
			},
			expectedResult: &CombinationDetails{
				CombinationID:      "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
				ShortCombinationID: "1reA6H5z4uFpotvegbLIr4",
				ModNames:           []string{"base"},
				Validation:         nil,
			},
		},
		"with validation": {
			combination: transfer.Combination{
				ID:             combinationID,
				ModNames:       []string{"base"},
				LastUsageTime:  time.Date(2038, 1, 19, 3, 14, 7, 0, time.UTC),
				ValidationTime: ptr(time.Date(2038, 1, 18, 3, 14, 7, 0, time.UTC)),
				ValidatedMods: map[string]modportalclient.Version{
					"foo": modportalclient.NewVersion("1.2.3"),
					"bar": modportalclient.NewVersion("2.3.4"),
				},
				ValidationProblems: []transfer.ValidationProblem{
					{
						ModName:    "foo",
						Type:       transfer.Conflict,
						Dependency: ptr(modportalclient.NewDependency("bar")),
					},
					{
						ModName: "baz",
						Type:    transfer.UnknownMod,
					},
				},
			},
			expectedResult: &CombinationDetails{
				CombinationID:      "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
				ShortCombinationID: "1reA6H5z4uFpotvegbLIr4",
				ModNames:           []string{"base"},
				Validation: &Validation{
					IsValid: false,
					ValidatedMods: []ValidatedMod{
						{
							Name:     "bar",
							Version:  ptr(modportalclient.NewVersion("2.3.4")),
							Problems: make([]ValidationProblem, 0),
						},
						{
							Name: "baz",
							Problems: []ValidationProblem{
								{
									Type: transfer.UnknownMod,
								},
							},
						},
						{
							Name:    "foo",
							Version: ptr(modportalclient.NewVersion("1.2.3")),
							Problems: []ValidationProblem{
								{
									Type:       transfer.Conflict,
									Dependency: ptr(modportalclient.NewDependency("bar")),
								},
							},
						},
					},
					ValidationTime: time.Date(2038, 1, 18, 3, 14, 7, 0, time.UTC),
				},
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			result := NewCombinationDetails(test.combination)

			assert.Equal(t, test.expectedResult, result)
		})
	}
}

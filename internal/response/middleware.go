package response

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/aws/aws-lambda-go/events"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
)

// NewProxyTransformMiddleware will transform the response and error from the handler to an APIGatewayProxyResponse.
// If the handler returns an error, an error response is created, containing the error's details.
// If the handler returns response which is not its zero-value, the response will be encoded as JSON into the proxy
// response body. If the handler returns the zero-value of the response, status 204 will be used instead.
func NewProxyTransformMiddleware[Request any, Response comparable](
	handler func(context.Context, Request) (Response, error),
) func(context.Context, Request) (events.APIGatewayProxyResponse, error) {
	return func(ctx context.Context, request Request) (events.APIGatewayProxyResponse, error) {
		response, err := handler(ctx, request)
		if err != nil {
			return createErrorResponse(err), nil
		}

		var zero Response
		if response == zero {
			return events.APIGatewayProxyResponse{
				StatusCode: http.StatusNoContent,
			}, nil
		}

		return createJsonResponse(http.StatusOK, response), nil
	}
}

// createJsonResponse will create an APIGatewayProxyResponse containing the provided data as JSON in the body, and
// using the provided status code.
func createJsonResponse(statusCode int, data any) events.APIGatewayProxyResponse {
	body, _ := json.Marshal(data)
	return events.APIGatewayProxyResponse{
		StatusCode: statusCode,
		Headers: map[string]string{
			"Content-Type": "application/json",
		},
		Body: string(body),
	}
}

// errorDetails is the ability to provide additional details of the error.
type errorDetails interface {
	ErrorType() errors.Type
	StatusCode() int
}

// errorResponse is the response structure used for errors when transforming to an APIGatewayProxyResponse.
type errorResponse struct {
	ErrorType    errors.Type `json:"errorType"`
	ErrorMessage string      `json:"errorMessage"`
}

// createErrorResponse creates a new APIGatewayProxyResponse response, setting it to the provided error.
// If the error supports ErrorType() and StatusCode(), those values will be used. Otherwise, the error type will be
// "unknown" and the status code will be 500.
func createErrorResponse(err error) events.APIGatewayProxyResponse {
	statusCode := http.StatusInternalServerError
	errorType := errors.TypeUnknown

	if ed, ok := err.(errorDetails); ok {
		statusCode = ed.StatusCode()
		errorType = ed.ErrorType()
	}

	return createJsonResponse(statusCode, errorResponse{
		ErrorType:    errorType,
		ErrorMessage: err.Error(),
	})
}

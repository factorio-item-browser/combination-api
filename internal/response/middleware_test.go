package response

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/aws/aws-lambda-go/events"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
)

type testResponse struct {
	Foo string `json:"foo"`
}

type testError struct {
	message    string
	statusCode int
	errorType  string
}

func (e *testError) Error() string {
	return e.message
}

func (e *testError) StatusCode() int {
	return e.statusCode
}

func (e *testError) ErrorType() errors.Type {
	return errors.Type(e.errorType)
}

func TestNewProxyTransformMiddleware(t *testing.T) {
	tests := map[string]struct {
		handlerResponse  any
		handlerError     error
		expectedResponse events.APIGatewayProxyResponse
	}{
		"ok": {
			handlerResponse: &testResponse{
				Foo: "bar",
			},
			handlerError: nil,
			expectedResponse: events.APIGatewayProxyResponse{
				StatusCode: http.StatusOK,
				Headers: map[string]string{
					"Content-Type": "application/json",
				},
				Body: `{"foo":"bar"}`,
			},
		},
		"no content": {
			handlerResponse: nil,
			handlerError:    nil,
			expectedResponse: events.APIGatewayProxyResponse{
				StatusCode: http.StatusNoContent,
			},
		},
		"known error": {
			handlerResponse: nil,
			handlerError: &testError{
				message:    "fancy error",
				statusCode: http.StatusTeapot,
				errorType:  "fancy",
			},
			expectedResponse: events.APIGatewayProxyResponse{
				StatusCode: http.StatusTeapot,
				Headers: map[string]string{
					"Content-Type": "application/json",
				},
				Body: `{"errorType":"fancy","errorMessage":"fancy error"}`,
			},
		},
		"internal error": {
			handlerResponse: nil,
			handlerError:    fmt.Errorf("test error"),
			expectedResponse: events.APIGatewayProxyResponse{
				StatusCode: http.StatusInternalServerError,
				Headers: map[string]string{
					"Content-Type": "application/json",
				},
				Body: `{"errorType":"unknown","errorMessage":"test error"}`,
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()
			request := "fancy-request"

			handler := func(c context.Context, r any) (any, error) {
				assert.Equal(t, ctx, c)
				assert.Equal(t, request, r)

				return test.handlerResponse, test.handlerError
			}

			instance := NewProxyTransformMiddleware(handler)
			response, err := instance(ctx, request)

			assert.Nil(t, err)
			if test.expectedResponse.Body != "" {
				assert.JSONEq(t, test.expectedResponse.Body, response.Body)

				test.expectedResponse.Body = ""
				response.Body = ""
			}
			assert.Equal(t, test.expectedResponse, response)
		})
	}
}

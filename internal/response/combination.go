package response

import (
	"slices"
	"strings"
	"time"

	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

type CombinationDetails struct {
	CombinationID      string      `json:"combinationID"`
	ShortCombinationID string      `json:"shortCombinationID"`
	ModNames           []string    `json:"modNames"`
	Validation         *Validation `json:"validation,omitempty"`
}

type Validation struct {
	IsValid        bool           `json:"isValid"`
	ValidatedMods  []ValidatedMod `json:"validatedMods"`
	ValidationTime time.Time      `json:"validationTime"`
}

type ValidatedMod struct {
	Name     string                   `json:"name"`
	Version  *modportalclient.Version `json:"version,omitempty"`
	Problems []ValidationProblem      `json:"problems,omitempty"`
}

type ValidationProblem struct {
	Type       transfer.ValidationProblemType `json:"type"`
	Dependency *modportalclient.Dependency    `json:"dependency,omitempty"`
}

func NewCombinationDetails(combination transfer.Combination) *CombinationDetails {
	return &CombinationDetails{
		CombinationID:      combination.ID.String(),
		ShortCombinationID: combination.ID.Short(),
		ModNames:           combination.ModNames,
		Validation:         mapValidation(combination),
	}
}

func mapValidation(combination transfer.Combination) *Validation {
	if combination.ValidationTime == nil {
		return nil
	}

	result := Validation{
		IsValid:        len(combination.ValidationProblems) == 0,
		ValidationTime: *combination.ValidationTime,
	}

	validatedMods := make(map[string]ValidatedMod, len(combination.ModNames))
	for modName, version := range combination.ValidatedMods {
		validatedMods[modName] = ValidatedMod{
			Name:     modName,
			Version:  &version,
			Problems: make([]ValidationProblem, 0),
		}
	}
	for _, problem := range combination.ValidationProblems {
		validatedMod, ok := validatedMods[problem.ModName]
		if !ok {
			validatedMod = ValidatedMod{
				Name:     problem.ModName,
				Version:  nil,
				Problems: make([]ValidationProblem, 0),
			}
		}

		validatedMod.Problems = append(validatedMod.Problems, ValidationProblem{
			Type:       problem.Type,
			Dependency: problem.Dependency,
		})
		validatedMods[problem.ModName] = validatedMod
	}

	result.ValidatedMods = make([]ValidatedMod, 0, len(validatedMods))
	for _, validatedMod := range validatedMods {
		result.ValidatedMods = append(result.ValidatedMods, validatedMod)
	}
	slices.SortFunc(result.ValidatedMods, func(a, b ValidatedMod) int {
		return strings.Compare(a.Name, b.Name)
	})

	return &result
}

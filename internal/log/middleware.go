package log

import (
	"context"
	"log/slog"
)

// NewMiddleware creates a new middleware which injects the provided logger into the context of the handler.
func NewMiddleware[Request any, Response any](
	logger *slog.Logger,
	handler func(ctx context.Context, request Request) (Response, error),
) func(ctx context.Context, request Request) (Response, error) {
	return func(ctx context.Context, request Request) (Response, error) {
		ctx = NewContext(ctx, logger)

		return handler(ctx, request)
	}
}

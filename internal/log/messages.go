package log

const (
	MessageAWSConfigError     = "aws config error"
	MessageCheckNameError     = "check name error"
	MessageEventHandleError   = "event handle error"
	MessageIncomingEvent      = "incoming event"
	MessageJSONUnmarshalError = "json unmarshal error"
	MessageResponse           = "response"
)

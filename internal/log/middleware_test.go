package log

import (
	"context"
	"fmt"
	"log/slog"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/config"
)

func TestNewMiddleware(t *testing.T) {
	cfg := config.Logger{
		Format: "json",
	}
	logger := New(cfg).With(slog.String("foo", "bar"))
	event := "wuppdi"
	response := "something"
	testError := fmt.Errorf("test error")

	handler := func(ctx context.Context, e any) (string, error) {
		assert.Same(t, logger, FromContext(ctx))
		assert.Equal(t, event, e)

		return response, testError
	}

	ctx := context.Background()

	instance := NewMiddleware(logger, handler)
	result, err := instance(ctx, event)

	assert.Equal(t, testError, err)
	assert.Equal(t, response, result)
}

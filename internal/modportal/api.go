package modportal

import (
	"context"
	"errors"
	"net/http"
	"slices"
	"sync"

	"gitlab.com/blue-psyduck/soushinki.git/pkg/client"
	serr "gitlab.com/blue-psyduck/soushinki.git/pkg/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/config"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
	"golang.org/x/sync/errgroup"
)

type API struct {
	modPortalClient *client.Client

	bundledModsNames []string
	workerCount      int
}

func NewAPI(cfg config.ModPortal) *API {
	return &API{
		modPortalClient:  modportalclient.New(),
		bundledModsNames: cfg.BundledModsNames,
		workerCount:      cfg.WorkerCount,
	}
}

// FactorioVersion will fetch and return the latest version currently available for Factorio.
func (a *API) FactorioVersion(ctx context.Context) (modportalclient.Version, error) {
	request := modportalclient.LatestFactorioReleasesRequest{}

	response, err := request.CastResponse(a.modPortalClient.Send(ctx, &request))
	if err != nil {
		return modportalclient.Version{}, err
	}

	return response.Experimental.Alpha, nil
}

// Mods will request the mods with the provided names from the Factorio Mod Portal. The returned map will contain all
// received mods by their name. Any not-available mods will be missing from the returned result.
func (a *API) Mods(ctx context.Context, modNames []string) (map[string]modportalclient.Mod, error) {
	var lock sync.Mutex
	mods := make(map[string]modportalclient.Mod, len(modNames))

	group, ctx := errgroup.WithContext(ctx)
	group.SetLimit(a.workerCount)
	for _, modName := range modNames {
		group.Go(func() error {
			mod, err := a.Mod(ctx, modName)
			if err == nil && mod != nil {
				lock.Lock()
				defer lock.Unlock()

				mods[mod.Name] = *mod
			}
			return err
		})
	}

	err := group.Wait()
	if err != nil {
		return nil, err
	}
	return mods, nil
}

// Mod requests the details from the mod with the provided name from the Factorio Mod Portal. If the mod is not known,
// nil without error will be returned.
func (a *API) Mod(ctx context.Context, modName string) (*modportalclient.Mod, error) {
	if slices.Contains(a.bundledModsNames, modName) {
		return nil, nil
	}

	request := modportalclient.FullModRequest{
		ModName: modName,
	}
	response, err := request.CastResponse(a.modPortalClient.Send(ctx, &request))
	if err != nil {
		var rse *serr.ResponseStatusError
		if errors.As(err, &rse) && rse.StatusCode() == http.StatusNotFound {
			return nil, nil
		}
		return nil, err
	}
	return &response.Mod, nil
}

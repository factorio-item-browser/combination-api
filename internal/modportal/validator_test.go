package modportal

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/config"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/modportal/mocks"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

func ptr[T any](t T) *T {
	return &t
}

func TestNewValidator(t *testing.T) {
	bundledModsDependencies := map[string][]string{
		"foo": {"bar"},
	}

	cfg := config.ModPortal{
		BundledModDependencies: bundledModsDependencies,
	}
	modsRequester := mocks.NewModsRequester(t)

	instance := NewValidator(cfg, modsRequester)

	assert.NotNil(t, instance)
	assert.Same(t, modsRequester, instance.modsRequester)
	assert.Equal(t, bundledModsDependencies, instance.bundledModDependencies)
}

func TestValidator_Validate(t *testing.T) {
	ctx := context.Background()

	dummyMod1 := modportalclient.Mod{
		Name: "dummy1",
		Releases: []modportalclient.Release{
			{
				Version: modportalclient.NewVersion("1.0.0"),
			},
		},
	}
	dummyMod2 := modportalclient.Mod{
		Name: "dummy2",
		Releases: []modportalclient.Release{
			{
				Version: modportalclient.NewVersion("2.0.0"),
			},
		},
	}
	missingReleaseMod := modportalclient.Mod{
		Name: "missingRelease",
	}
	validMod := modportalclient.Mod{
		Name: "valid",
		Releases: []modportalclient.Release{
			{
				Version: modportalclient.NewVersion("3.4.5"),
				InfoJson: modportalclient.InfoJson{
					Dependencies: []modportalclient.Dependency{
						modportalclient.NewDependency("base"),
						modportalclient.NewDependency("dummy1"),
						modportalclient.NewDependency("dummy2 >= 1.5"),
						modportalclient.NewDependency("? notExisting1"),
						modportalclient.NewDependency("! notExisting2"),
					},
				},
			},
		},
	}
	missingDependencyMod1 := modportalclient.Mod{
		Name: "missingDependency1",
		Releases: []modportalclient.Release{
			{
				Version: modportalclient.NewVersion("4.5.6"),
				InfoJson: modportalclient.InfoJson{
					Dependencies: []modportalclient.Dependency{
						modportalclient.NewDependency("notExisting"),
						modportalclient.NewDependency("dummy1 > 2.0"),
					},
				},
			},
		},
	}
	missingDependencyMod2 := modportalclient.Mod{
		Name: "missingDependency2",
		Releases: []modportalclient.Release{
			{
				Version: modportalclient.NewVersion("4.5.6"),
				InfoJson: modportalclient.InfoJson{
					Dependencies: []modportalclient.Dependency{
						modportalclient.NewDependency("notExisting"),
						modportalclient.NewDependency("~ dummy1 > 2.0"),
					},
				},
			},
		},
	}
	conflictedMod := modportalclient.Mod{
		Name: "conflicted",
		Releases: []modportalclient.Release{
			{
				Version: modportalclient.NewVersion("5.6.7"),
				InfoJson: modportalclient.InfoJson{
					Dependencies: []modportalclient.Dependency{
						modportalclient.NewDependency("! dummy1"),
						modportalclient.NewDependency("! dummy2"),
					},
				},
			},
		},
	}

	factorioVersion := modportalclient.NewVersion("1.2.3")
	modNames := []string{
		"base",
		"quality",
		"dummy1",
		"dummy2",
		"missing",
		"missingRelease",
		"valid",
		"missingDependency1",
		"missingDependency2",
		"conflicted",
	}
	mods := map[string]modportalclient.Mod{
		"dummy1":             dummyMod1,
		"dummy2":             dummyMod2,
		"missingRelease":     missingReleaseMod,
		"valid":              validMod,
		"missingDependency1": missingDependencyMod1,
		"missingDependency2": missingDependencyMod2,
		"conflicted":         conflictedMod,
	}
	expectedValidatedMods := map[string]modportalclient.Version{
		"base":               modportalclient.NewVersion("1.2.3"),
		"quality":            modportalclient.NewVersion("1.2.3"),
		"dummy1":             modportalclient.NewVersion("1.0.0"),
		"dummy2":             modportalclient.NewVersion("2.0.0"),
		"valid":              modportalclient.NewVersion("3.4.5"),
		"missingDependency1": modportalclient.NewVersion("4.5.6"),
		"missingDependency2": modportalclient.NewVersion("4.5.6"),
		"conflicted":         modportalclient.NewVersion("5.6.7"),
	}
	expectedValidationProblems := []transfer.ValidationProblem{
		{
			ModName: "missing",
			Type:    transfer.UnknownMod,
		},
		{
			ModName: "missingRelease",
			Type:    transfer.NoRelease,
		},
		{
			ModName:    "missingDependency1",
			Type:       transfer.MissingDependency,
			Dependency: ptr(modportalclient.NewDependency("notExisting")),
		},
		{
			ModName:    "missingDependency1",
			Type:       transfer.MissingDependency,
			Dependency: ptr(modportalclient.NewDependency("dummy1 > 2.0")),
		},
		{
			ModName:    "missingDependency2",
			Type:       transfer.MissingDependency,
			Dependency: ptr(modportalclient.NewDependency("notExisting")),
		},
		{
			ModName:    "missingDependency2",
			Type:       transfer.MissingDependency,
			Dependency: ptr(modportalclient.NewDependency("~ dummy1 > 2.0")),
		},
		{
			ModName:    "conflicted",
			Type:       transfer.Conflict,
			Dependency: ptr(modportalclient.NewDependency("! dummy1")),
		},
		{
			ModName:    "conflicted",
			Type:       transfer.Conflict,
			Dependency: ptr(modportalclient.NewDependency("! dummy2")),
		},
	}

	modsRequester := mocks.NewModsRequester(t)
	modsRequester.On("Mods", ctx, modNames).Once().Return(mods, nil)

	instance := Validator{
		modsRequester: modsRequester,
		bundledModDependencies: map[string][]string{
			"base":    {},
			"quality": {"base"},
		},
	}
	validatedMods, validationProblems, err := instance.Validate(ctx, modNames, factorioVersion)

	assert.Nil(t, err)
	assert.Equal(t, expectedValidatedMods, validatedMods)
	assert.Equal(t, expectedValidationProblems, validationProblems)
}

func TestValidator_Validate_withError(t *testing.T) {
	ctx := context.Background()
	modNames := []string{"base", "dummy1", "dummy2"}
	factorioVersion := modportalclient.NewVersion("1.2.3")

	modsRequester := mocks.NewModsRequester(t)
	modsRequester.On("Mods", ctx, modNames).Once().Return(nil, fmt.Errorf("test error"))

	instance := Validator{
		modsRequester: modsRequester,
	}
	_, _, err := instance.Validate(ctx, modNames, factorioVersion)

	assert.NotNil(t, err)
}

package modportal

import (
	"context"

	"gitlab.com/factorio-item-browser/combination-api.git/internal/config"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

type modsRequester interface {
	Mods(ctx context.Context, modNames []string) (map[string]modportalclient.Mod, error)
}

type Validator struct {
	modsRequester modsRequester

	bundledModDependencies map[string][]string
}

func NewValidator(cfg config.ModPortal, modsRequester modsRequester) *Validator {
	return &Validator{
		modsRequester:          modsRequester,
		bundledModDependencies: cfg.BundledModDependencies,
	}
}

func (v *Validator) Validate(
	ctx context.Context,
	modNames []string,
	factorioVersion modportalclient.Version,
) (map[string]modportalclient.Version, []transfer.ValidationProblem, error) {
	// Read additional mods from the mod portal.
	mods, err := v.modsRequester.Mods(ctx, modNames)
	if err != nil {
		return nil, nil, err
	}

	// Add bundled mod if required.
	for _, modName := range modNames {
		if dependencies, ok := v.bundledModDependencies[modName]; ok {
			mods[modName] = createBundledMod(modName, dependencies, factorioVersion)
		}
	}

	// Select the release of each mod.
	releases := make(map[string]modportalclient.Release, len(mods))
	for name, mod := range mods {
		release := mod.SelectLatestRelease(&factorioVersion)
		if release != nil {
			releases[name] = *release
		}
	}

	// Check form validation problems.
	validatedMods := make(map[string]modportalclient.Version, len(modNames))
	var validationProblems []transfer.ValidationProblem
	for _, modName := range modNames {
		if _, ok := mods[modName]; !ok {
			validationProblems = append(validationProblems, transfer.ValidationProblem{
				ModName: modName,
				Type:    transfer.UnknownMod,
			})
			continue
		}

		release, ok := releases[modName]
		if !ok {
			validationProblems = append(validationProblems, transfer.ValidationProblem{
				ModName: modName,
				Type:    transfer.NoRelease,
			})
			continue
		}

		validatedMods[modName] = release.Version
		validationProblems = append(validationProblems, v.validateRelease(ctx, modName, release, releases)...)
	}

	return validatedMods, validationProblems, nil
}

func createBundledMod(name string, dependencies []string, version modportalclient.Version) modportalclient.Mod {
	mappedDependencies := make([]modportalclient.Dependency, 0, len(dependencies))
	for _, dependency := range dependencies {
		mappedDependencies = append(mappedDependencies, modportalclient.Dependency{
			Type:     modportalclient.DependencyTypeMandatory,
			ModName:  dependency,
			Operator: modportalclient.DependencyOperatorAny,
		})
	}

	return modportalclient.Mod{
		Name: name,
		Releases: []modportalclient.Release{
			{
				Version: version,
				InfoJson: modportalclient.InfoJson{
					Dependencies: mappedDependencies,
				},
			},
		},
	}
}

func (v *Validator) validateRelease(
	_ context.Context,
	modName string,
	release modportalclient.Release,
	allReleases map[string]modportalclient.Release,
) []transfer.ValidationProblem {
	var problems []transfer.ValidationProblem
	for _, dependency := range release.InfoJson.Dependencies {
		dependentRelease, ok := allReleases[dependency.ModName]
		switch dependency.Type {
		case modportalclient.DependencyTypeMandatory:
			fallthrough
		case modportalclient.DependencyTypeMandatoryCircular:
			if !ok || !dependency.MatchesVersion(dependentRelease.Version) {
				problems = append(problems, transfer.ValidationProblem{
					ModName:    modName,
					Type:       transfer.MissingDependency,
					Dependency: &dependency,
				})
			}

		case modportalclient.DependencyTypeConflict:
			if ok {
				problems = append(problems, transfer.ValidationProblem{
					ModName:    modName,
					Type:       transfer.Conflict,
					Dependency: &dependency,
				})
			}
		}
	}

	return problems
}

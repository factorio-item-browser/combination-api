package modportal

import (
	"context"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/clienttest"
	"gitlab.com/blue-psyduck/soushinki.git/pkg/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/config"
	"gitlab.com/factorio-item-browser/factorio-mod-portal-client.git/pkg/modportalclient"
)

func TestNewAPI(t *testing.T) {
	cfg := config.ModPortal{
		BundledModsNames: []string{"foo", "bar"},
		WorkerCount:      1337,
	}

	instance := NewAPI(cfg)

	assert.NotNil(t, instance)
	assert.NotNil(t, instance.modPortalClient)
	assert.Equal(t, instance.bundledModsNames, []string{"foo", "bar"})
	assert.Equal(t, instance.workerCount, 1337)
}

func TestAPI_FactorioVersion(t *testing.T) {
	tests := map[string]struct {
		apiResponse       *modportalclient.LatestFactorioReleasesResponse
		apiError          error
		expectedErrorType error
		expectedResult    modportalclient.Version
	}{
		"happy path": {
			apiResponse: &modportalclient.LatestFactorioReleasesResponse{
				Experimental: modportalclient.LatestRelease{
					Alpha: modportalclient.NewVersion("1.2.3"),
				},
			},
			apiError:          nil,
			expectedErrorType: nil,
			expectedResult:    modportalclient.NewVersion("1.2.3"),
		},
		"api error": {
			apiResponse:       nil,
			apiError:          fmt.Errorf("test error"),
			expectedErrorType: &errors.ClientError{},
			expectedResult:    modportalclient.Version{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()

			builder := clienttest.NewMockedClientBuilder()
			builder.On(&modportalclient.LatestFactorioReleasesRequest{}).Once().Return(test.apiResponse, test.apiError)

			instance := API{
				modPortalClient: builder.Client(),
			}
			result, err := instance.FactorioVersion(ctx)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestAPI_Mods(t *testing.T) {
	ctx := context.Background()
	modNames := []string{"foo", "bar", "base", "baz"}
	modFoo := modportalclient.Mod{
		Name:  "foo",
		Owner: "oof",
	}
	modBar := modportalclient.Mod{
		Name:  "bar",
		Owner: "rab",
	}
	expectedResult := map[string]modportalclient.Mod{
		"foo": modFoo,
		"bar": modBar,
	}

	builder := clienttest.NewMockedClientBuilder()
	builder.On(&modportalclient.FullModRequest{ModName: "foo"}).Once().Return(&modportalclient.ModResponse{Mod: modFoo}, nil)
	builder.On(&modportalclient.FullModRequest{ModName: "bar"}).Once().Return(&modportalclient.ModResponse{Mod: modBar}, nil)
	builder.On(&modportalclient.FullModRequest{ModName: "baz"}).Once().Return(nil, errors.NewResponseStatusError(http.StatusNotFound, nil, nil, nil))

	instance := API{
		modPortalClient:  builder.Client(),
		bundledModsNames: []string{"base"},
		workerCount:      2,
	}
	result, err := instance.Mods(ctx, modNames)

	assert.Nil(t, err)
	assert.Equal(t, expectedResult, result)
}

func TestAPI_Mods_withError(t *testing.T) {
	ctx := context.Background()
	modNames := []string{"foo", "bar", "base", "baz"}
	modFoo := modportalclient.Mod{
		Name:  "foo",
		Owner: "oof",
	}
	modBar := modportalclient.Mod{
		Name:  "bar",
		Owner: "rab",
	}

	builder := clienttest.NewMockedClientBuilder()
	builder.On(&modportalclient.FullModRequest{ModName: "foo"}).Once().Return(&modportalclient.ModResponse{Mod: modFoo}, nil)
	builder.On(&modportalclient.FullModRequest{ModName: "bar"}).Once().Return(&modportalclient.ModResponse{Mod: modBar}, nil)
	builder.On(&modportalclient.FullModRequest{ModName: "baz"}).Once().Return(nil, fmt.Errorf("test error"))

	instance := API{
		modPortalClient:  builder.Client(),
		bundledModsNames: []string{"base"},
		workerCount:      2,
	}
	_, err := instance.Mods(ctx, modNames)

	assert.IsType(t, &errors.ClientError{}, err)
}

func TestAPI_Mod(t *testing.T) {
	mod := modportalclient.Mod{
		Name:  "foo",
		Owner: "bar",
	}

	tests := map[string]struct {
		modName           string
		expectedRequest   *modportalclient.FullModRequest
		apiResponse       *modportalclient.ModResponse
		apiError          error
		expectedErrorType error
		expectedResult    *modportalclient.Mod
	}{
		"happy path": {
			modName:         "foo",
			expectedRequest: &modportalclient.FullModRequest{ModName: "foo"},
			apiResponse: &modportalclient.ModResponse{
				Mod: mod,
			},
			apiError:          nil,
			expectedErrorType: nil,
			expectedResult:    &mod,
		},
		"bundled mod": {
			modName:           "base",
			expectedRequest:   nil,
			apiResponse:       nil,
			apiError:          nil,
			expectedErrorType: nil,
			expectedResult:    nil,
		},
		"not found error": {
			modName:           "foo",
			expectedRequest:   &modportalclient.FullModRequest{ModName: "foo"},
			apiResponse:       nil,
			apiError:          errors.NewResponseStatusError(http.StatusNotFound, nil, nil, nil),
			expectedErrorType: nil,
			expectedResult:    nil,
		},
		"api error": {
			modName:           "foo",
			expectedRequest:   &modportalclient.FullModRequest{ModName: "foo"},
			apiResponse:       nil,
			apiError:          fmt.Errorf("test error"),
			expectedErrorType: &errors.ClientError{},
			expectedResult:    nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()

			builder := clienttest.NewMockedClientBuilder()
			if test.expectedRequest != nil {
				builder.On(test.expectedRequest).Once().Return(test.apiResponse, test.apiError)
			}

			instance := API{
				modPortalClient:  builder.Client(),
				bundledModsNames: []string{"base"},
			}
			result, err := instance.Mod(ctx, test.modName)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

package config

import (
	"os"
	"strconv"
	"strings"
)

type ModPortal struct {
	BundledModsNames       []string
	BundledModDependencies map[string][]string
	WorkerCount            int
}

func ForModPortal() ModPortal {
	workerCount, _ := strconv.ParseInt(os.Getenv("MOD_PORTAL_WORKER_COUNT"), 10, 64)
	bundledModNames, bundledModDependencies := parseBundledMods(os.Getenv("MOD_PORTAL_BUNDLED_MODS"))

	return ModPortal{
		BundledModsNames:       bundledModNames,
		BundledModDependencies: bundledModDependencies,
		WorkerCount:            int(workerCount),
	}
}

func parseBundledMods(value string) (modNames []string, modDependencies map[string][]string) {
	modDependencies = make(map[string][]string)
	for _, mod := range strings.Split(value, "|") {
		parts := strings.Split(mod, ":")
		modName := parts[0]
		dependencies := make([]string, 0)

		if len(parts) >= 2 {
			dependencies = strings.Split(parts[1], ",")
		}

		modNames = append(modNames, modName)
		modDependencies[modName] = dependencies
	}
	return
}

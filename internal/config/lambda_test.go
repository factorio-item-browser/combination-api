package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestForLambda(t *testing.T) {
	tests := map[string]struct {
		env            map[string]string
		expectedResult Lambda
	}{
		"lambda": {
			env: map[string]string{
				"AWS_LAMBDA_FUNCTION_NAME": "test",
			},
			expectedResult: Lambda{
				IsLambda: true,
			},
		},
		"cli": {
			env: map[string]string{
				"AWS_LAMBDA_FUNCTION_NAME": "",
			},
			expectedResult: Lambda{
				IsLambda: false,
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			currentEnv := make(map[string]string, len(test.env))
			for k := range test.env {
				currentEnv[k] = os.Getenv(k)
			}
			defer func(env map[string]string) {
				for k, v := range env {
					_ = os.Setenv(k, v)
				}
			}(currentEnv)
			for k, v := range test.env {
				_ = os.Setenv(k, v)
			}

			result := ForLambda()
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

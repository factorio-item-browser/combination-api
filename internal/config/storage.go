package config

import "os"

type Storage struct {
	TableName string
	IndexName string
}

func ForStorage() Storage {
	return Storage{
		TableName: os.Getenv("STORAGE_TABLE_NAME"),
		IndexName: os.Getenv("STORAGE_INDEX_NAME"),
	}
}

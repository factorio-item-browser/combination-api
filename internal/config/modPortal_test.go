package config

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestForModPortal(t *testing.T) {
	env := map[string]string{
		"MOD_PORTAL_BUNDLED_MODS": "base|elevated-rails:base|quality:base|space-age:base,elevated-rails,quality",
		"MOD_PORTAL_WORKER_COUNT": "1337",
	}
	expectedResult := ModPortal{
		BundledModsNames: []string{"base", "elevated-rails", "quality", "space-age"},
		BundledModDependencies: map[string][]string{
			"base":           {},
			"elevated-rails": {"base"},
			"quality":        {"base"},
			"space-age":      {"base", "elevated-rails", "quality"},
		},
		WorkerCount: 1337,
	}

	currentEnv := make(map[string]string, len(env))
	for k := range env {
		currentEnv[k] = os.Getenv(k)
	}
	defer func(env map[string]string) {
		for k, v := range env {
			_ = os.Setenv(k, v)
		}
	}(currentEnv)
	for k, v := range env {
		_ = os.Setenv(k, v)
	}

	result := ForModPortal()
	assert.Equal(t, expectedResult, result)
}

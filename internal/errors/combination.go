package errors

import (
	"fmt"
	"net/http"
)

type InvalidCombinationIDError struct {
	combinationID string
}

func NewInvalidCombinationIDError(combinationID string) *InvalidCombinationIDError {
	return &InvalidCombinationIDError{
		combinationID: combinationID,
	}
}

func (e *InvalidCombinationIDError) Error() string {
	return fmt.Sprintf("invalid combination id: %s", e.combinationID)
}

func (e *InvalidCombinationIDError) StatusCode() int {
	return http.StatusBadRequest
}

func (e *InvalidCombinationIDError) ErrorType() Type {
	return TypeInvalidCombinationID
}

type CombinationNotKnownError struct {
	combinationID string
}

func NewCombinationNotKnownError(combinationID string) *CombinationNotKnownError {
	return &CombinationNotKnownError{
		combinationID: combinationID,
	}
}

func (e *CombinationNotKnownError) Error() string {
	return fmt.Sprintf("combination not known: %s", e.combinationID)
}

func (e *CombinationNotKnownError) StatusCode() int {
	return http.StatusNotFound
}

func (e *CombinationNotKnownError) ErrorType() Type {
	return TypeCombinationNotKnown
}

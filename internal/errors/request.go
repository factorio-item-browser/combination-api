package errors

import (
	"fmt"
	"net/http"
)

type InvalidRequestBodyError struct {
	originalError error
}

func NewInvalidRequestBodyError(originalError error) *InvalidRequestBodyError {
	return &InvalidRequestBodyError{
		originalError: originalError,
	}
}

func (e *InvalidRequestBodyError) Error() string {
	return fmt.Sprintf("invalid request body: %s", e.originalError)
}

func (e *InvalidRequestBodyError) StatusCode() int {
	return http.StatusBadRequest
}

func (e *InvalidRequestBodyError) ErrorType() Type {
	return TypeInvalidRequestBody
}

func (e *InvalidRequestBodyError) Unwrap() error {
	return e.originalError
}

package errors

type Type string

const (
	TypeCombinationNotKnown  Type = "combination-not-known"
	TypeInvalidCombinationID Type = "invalid-combination-id"
	TypeInvalidRequestBody   Type = "invalid-request-body"
	TypeUnknown              Type = "unknown"
)

package errors

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInvalidCombinationIDError(t *testing.T) {
	combinationID := "foo"

	instance := NewInvalidCombinationIDError(combinationID)

	assert.NotNil(t, instance)
	assert.Equal(t, "invalid combination id: foo", instance.Error())
	assert.Equal(t, http.StatusBadRequest, instance.StatusCode())
	assert.Equal(t, TypeInvalidCombinationID, instance.ErrorType())
}

func TestCombinationNotKnownError(t *testing.T) {
	combinationID := "foo"

	instance := NewCombinationNotKnownError(combinationID)

	assert.NotNil(t, instance)
	assert.Equal(t, "combination not known: foo", instance.Error())
	assert.Equal(t, http.StatusNotFound, instance.StatusCode())
	assert.Equal(t, TypeCombinationNotKnown, instance.ErrorType())
}

package errors

import "fmt"

type StorageError struct {
	originalError error
}

func NewStorageError(originalError error) *StorageError {
	return &StorageError{
		originalError: originalError,
	}
}

func (e *StorageError) Error() string {
	return fmt.Sprintf("storage error: %s", e.originalError)
}

func (e *StorageError) Unwrap() error {
	return e.originalError
}

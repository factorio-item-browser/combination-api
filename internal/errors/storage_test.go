package errors

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestStorageError(t *testing.T) {
	originalError := fmt.Errorf("test error")

	instance := NewStorageError(originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, "storage error: test error", instance.Error())
	assert.Equal(t, originalError, instance.Unwrap())
}

package errors

import (
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInvalidRequestBodyError(t *testing.T) {
	originalError := fmt.Errorf("test error")

	instance := NewInvalidRequestBodyError(originalError)

	assert.NotNil(t, instance)
	assert.Equal(t, "invalid request body: test error", instance.Error())
	assert.Equal(t, http.StatusBadRequest, instance.StatusCode())
	assert.Equal(t, TypeInvalidRequestBody, instance.ErrorType())
	assert.Equal(t, originalError, instance.Unwrap())
}

package combinationid

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/combinationid/mocks"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
)

func TestNewCalculator(t *testing.T) {
	idByNameFetcher := mocks.NewIdByNameFetcher(t)

	instance := NewCalculator(idByNameFetcher)

	assert.NotNil(t, instance)
	assert.Same(t, idByNameFetcher, instance.idByNameFetcher)
}

func TestCalculator_CalculateFromString(t *testing.T) {
	combinationID := transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76"))

	tests := map[string]struct {
		value             string
		expectNameFetch   bool
		nameFetchResult   *transfer.CombinationID
		nameFetchError    error
		expectedErrorType error
		expectedResult    transfer.CombinationID
	}{
		"valid": {
			value:             "2f4a45fa-a509-a9d1-aae6-ffcf984a7a76",
			expectNameFetch:   false,
			expectedErrorType: nil,
			expectedResult:    combinationID,
		},
		"valid without dashes": {
			value:             "2f4a45faa509a9d1aae6ffcf984a7a76",
			expectNameFetch:   false,
			expectedErrorType: nil,
			expectedResult:    combinationID,
		},
		"valid short": {
			value:             "1reA6H5z4uFpotvegbLIr4",
			expectNameFetch:   false,
			expectedErrorType: nil,
			expectedResult:    combinationID,
		},
		"valid name": {
			value:             "vanilla",
			expectNameFetch:   true,
			nameFetchResult:   &combinationID,
			nameFetchError:    nil,
			expectedErrorType: nil,
			expectedResult:    combinationID,
		},
		"unknown name": {
			value:             "vanilla",
			expectNameFetch:   true,
			nameFetchResult:   nil,
			nameFetchError:    nil,
			expectedErrorType: &errors.InvalidCombinationIDError{},
			expectedResult:    transfer.CombinationID{},
		},
		"invalid": {
			value:             "2f4a45fa-xxxx-a9d1-aae6-ffcf984a7a76",
			expectNameFetch:   true,
			nameFetchResult:   nil,
			nameFetchError:    nil,
			expectedErrorType: &errors.InvalidCombinationIDError{},
			expectedResult:    transfer.CombinationID{},
		},
		"invalid short": {
			value:             "13245678901234567890-=",
			expectNameFetch:   true,
			nameFetchResult:   nil,
			nameFetchError:    nil,
			expectedErrorType: &errors.InvalidCombinationIDError{},
			expectedResult:    transfer.CombinationID{},
		},
		"short overflow": {
			value:             "ZZZZZZZZZZZZZZZZZZZZZZ",
			expectNameFetch:   true,
			nameFetchResult:   nil,
			nameFetchError:    nil,
			expectedErrorType: &errors.InvalidCombinationIDError{},
			expectedResult:    transfer.CombinationID{},
		},
		"name fetch error": {
			value:             "vanilla",
			expectNameFetch:   true,
			nameFetchResult:   nil,
			nameFetchError:    fmt.Errorf("test error"),
			expectedErrorType: &errors.InvalidCombinationIDError{},
			expectedResult:    transfer.CombinationID{},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()

			idByNameFetcher := mocks.NewIdByNameFetcher(t)
			if test.expectNameFetch {
				idByNameFetcher.
					On("CombinationIDByName", ctx, test.value).
					Once().
					Return(test.nameFetchResult, test.nameFetchError)
			}

			instance := Calculator{
				idByNameFetcher: idByNameFetcher,
				parsers: []stringParser{
					(*Calculator).parseFullID,
					(*Calculator).parseShortID,
					(*Calculator).parseName,
				},
			}
			result, err := instance.CalculateFromString(ctx, test.value)

			assert.IsType(t, test.expectedErrorType, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

func TestCalculator_CalculateFromModNames(t *testing.T) {
	tests := map[string]struct {
		modNames       []string
		expectedResult transfer.CombinationID
	}{
		"vanilla": {
			modNames:       []string{"base"},
			expectedResult: transfer.NewCombinationID(uuid.MustParse("2f4a45fa-a509-a9d1-aae6-ffcf984a7a76")),
		},
		"multiple mods": {
			modNames:       []string{"Bar", "Foo", "base"},
			expectedResult: transfer.NewCombinationID(uuid.MustParse("83f899a6-ba39-8f56-a704-92907650dc3e")),
		},
		"multiple mods shuffled": {
			modNames:       []string{"base", "Bar", "Foo"},
			expectedResult: transfer.NewCombinationID(uuid.MustParse("83f899a6-ba39-8f56-a704-92907650dc3e")),
		},
		"duplicate mod": {
			modNames:       []string{"Bar", "base", "Foo", "base"},
			expectedResult: transfer.NewCombinationID(uuid.MustParse("83f899a6-ba39-8f56-a704-92907650dc3e")),
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			ctx := context.Background()

			instance := Calculator{}
			result, err := instance.CalculateFromModNames(ctx, test.modNames)

			assert.Nil(t, err)
			assert.Equal(t, test.expectedResult, result)
		})
	}
}

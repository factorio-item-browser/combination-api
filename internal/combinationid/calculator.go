package combinationid

import (
	"context"
	"crypto/md5"
	"encoding/json"
	"log/slog"
	"math/big"
	"sort"
	"strings"

	"github.com/google/uuid"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/errors"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/log"
	"gitlab.com/factorio-item-browser/combination-api.git/internal/transfer"
)

type idByNameFetcher interface {
	CombinationIDByName(ctx context.Context, name string) (*transfer.CombinationID, error)
}

type stringParser func(*Calculator, context.Context, string) *transfer.CombinationID

type Calculator struct {
	idByNameFetcher idByNameFetcher
	parsers         []stringParser
}

func NewCalculator(idByNameFetcher idByNameFetcher) *Calculator {
	return &Calculator{
		idByNameFetcher: idByNameFetcher,
		parsers: []stringParser{
			(*Calculator).parseFullID,
			(*Calculator).parseShortID,
			(*Calculator).parseName,
		},
	}
}

// CalculateFromString calculates the combination id from the provided string value.
// The value can either be the full id in its UUID format, or its short representation using base62.
func (c *Calculator) CalculateFromString(ctx context.Context, value string) (transfer.CombinationID, error) {
	for _, parser := range c.parsers {
		id := parser(c, ctx, value)
		if id != nil {
			return *id, nil
		}
	}

	return transfer.CombinationID{}, errors.NewInvalidCombinationIDError(value)
}

// parseFullID attempts to parse the provided value as a full-length combination id.
func (c *Calculator) parseFullID(_ context.Context, value string) *transfer.CombinationID {
	parsedID, err := uuid.Parse(value)
	if err != nil {
		return nil
	}

	id := transfer.NewCombinationID(parsedID)
	return &id
}

// parseShortID attempts to parse the provided value as a short combination id, using base62 encoding.
func (c *Calculator) parseShortID(_ context.Context, value string) *transfer.CombinationID {
	if len(value) != 22 {
		// Short ids have a fixed length of 22 characters.
		return nil
	}

	var i big.Int
	if _, ok := i.SetString(value, 62); !ok {
		return nil
	}

	parsedID, err := uuid.FromBytes(i.Bytes())
	if err != nil {
		return nil
	}

	id := transfer.NewCombinationID(parsedID)
	return &id
}

// parseName parses the value as a named combination, checking against the storage.
func (c *Calculator) parseName(ctx context.Context, value string) *transfer.CombinationID {
	id, err := c.idByNameFetcher.CombinationIDByName(ctx, value)
	if err != nil {
		log.FromContext(ctx).Error(log.MessageCheckNameError, slog.String("error", err.Error()))
		return nil
	}

	return id
}

// CalculateFromModNames calculates the combination id from the provided list of mod names.
func (c *Calculator) CalculateFromModNames(_ context.Context, modNames []string) (transfer.CombinationID, error) {
	distinctModNames := make(map[string]struct{}, len(modNames))
	for _, modName := range modNames {
		modName = strings.TrimSpace(modName)
		if _, ok := distinctModNames[modName]; ok {
			continue
		}
		distinctModNames[modName] = struct{}{}
	}

	realModNames := make([]string, 0, len(distinctModNames))
	for modName := range distinctModNames {
		realModNames = append(realModNames, modName)
	}
	sort.Strings(realModNames)

	data, _ := json.Marshal(realModNames)
	hash := md5.Sum(data)
	id, _ := uuid.FromBytes(hash[:])

	return transfer.NewCombinationID(id), nil
}
